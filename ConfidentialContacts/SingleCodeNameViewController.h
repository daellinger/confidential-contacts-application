//
//  SingleCodeNameViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/12/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "StaticDataTableViewController.h"
#import "CodeName.h"
#import <Parse.h>
#import "DTAlertView.h"
#import "Helper.h"

@interface SingleCodeNameViewController : StaticDataTableViewController <UITextFieldDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) CodeName *codeName;

- (IBAction)backToCodename:(UIBarButtonItem *)sender;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;

- (IBAction)deleteCodeName:(UIButton *)sender;
- (IBAction)editCodeName:(UIBarButtonItem *)sender;


@property (strong, nonatomic) IBOutlet UITableViewCell *deleteCodenameCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *descriptionCell;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;

@property (strong, nonatomic) NSArray *codeNames; //Used to determine if the edit is a duplicate

@end

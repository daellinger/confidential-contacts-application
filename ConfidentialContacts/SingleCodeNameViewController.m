//
//  SingleCodeNameViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/12/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "SingleCodeNameViewController.h"

@interface SingleCodeNameViewController ()

@property (nonatomic,assign) BOOL isEditing;

@end

@implementation SingleCodeNameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [Helper makeTextViewRounded:self.descriptionTextView];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"SingleCodeName - viewDidLoad method: codename = %@",self.codeName.name);
    [self populateFieldsAndHideEmptyCells];

    UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonSystemItemDone target:self action:@selector(editCodeName:)];
    self.navigationItem.rightBarButtonItem = newButton;
    _editButton = newButton;
    self.isEditing = NO;

}

- (void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
}

- (IBAction)backToCodename:(UIBarButtonItem *)sender {
    if ([self isEditing]) {
        NSLog(@"Turning edit mode off");
        [self populateFieldsAndHideEmptyCells];
        UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonSystemItemDone target:self action:@selector(editCodeName:)];
        self.navigationItem.rightBarButtonItem = newButton;
        _editButton = newButton;
        self.isEditing = NO;
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Delete Contact Methods

- (IBAction)deleteCodeName:(UIButton *)sender {
    NSLog(@"Entered deleteContact method");
    
    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        // You can get button title of clicked button.
        NSLog(@"User clicked on %@", _alertView.clickedButtonTitle);
        if([_alertView.clickedButtonTitle isEqualToString:@"Yes, Delete"]){
            [self deleteCodeNameFromParse];
        }
        
    };
    DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Confirm Deletion"
                                                    message:@"Delete this Code Name? All associated connections and messages will also be deleted."
                                          cancelButtonTitle:@"No" positiveButtonTitle:@"Yes, Delete"];
    [alertView show];
}

- (void)deleteCodeNameFromParse {
    NSString *codeNameId = self.codeName.parseObjectId;
    NSString *codeName = [self.codeName name];
    
    PFQuery *query = [PFQuery queryWithClassName:@"CodeName"];
    [query whereKey:@"objectId" equalTo:codeNameId];
    query.cachePolicy = kPFCachePolicyCacheElseNetwork;
    
    PFQuery *connectionQuery1 = [PFQuery queryWithClassName:@"Connection"];
    connectionQuery1.cachePolicy = kPFCachePolicyCacheElseNetwork;
    [connectionQuery1 whereKey:@"inviterObjectId" equalTo:codeNameId];
    PFQuery *connectionQuery2 = [PFQuery queryWithClassName:@"Connection"];
    connectionQuery2.cachePolicy = kPFCachePolicyCacheElseNetwork;
    [connectionQuery2 whereKey:@"inviteeObjectId" equalTo:codeNameId];
    PFQuery *connectionQuery = [PFQuery orQueryWithSubqueries:@[connectionQuery1,connectionQuery2]];
    connectionQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
    
    PFQuery *chatQuery1 = [PFQuery queryWithClassName:@"Chat"];
    chatQuery1.cachePolicy = kPFCachePolicyCacheElseNetwork;
    [chatQuery1 whereKey:@"fromObjectId" equalTo:codeNameId];
    PFQuery *chatQuery2 = [PFQuery queryWithClassName:@"Chat"];
    chatQuery2.cachePolicy = kPFCachePolicyCacheElseNetwork;
    [chatQuery2 whereKey:@"toObjectId" equalTo:codeNameId];
    PFQuery *chatQuery = [PFQuery orQueryWithSubqueries:@[chatQuery1,chatQuery2]];
    chatQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
    
    NSMutableArray *objectsToDelete = [[NSMutableArray alloc] init];
    
    NSLog(@"object id for parse codename: %@",codeNameId);
    
    PFObject *codeNameToDelete = [query getObjectWithId:codeNameId];
    
    NSArray *chatsToDelete = [chatQuery findObjects];
    if(chatsToDelete != nil && [chatsToDelete count] > 0){
        NSLog(@"Retrieved %d chat objects assigned for deletion from 'Chat' table", [chatsToDelete count]);
        [objectsToDelete addObjectsFromArray:chatsToDelete];
    }
    
    NSArray *connectionsToDelete = [connectionQuery findObjects];
    if(connectionsToDelete != nil && [connectionsToDelete count] > 0){
        NSLog(@"Retrieved %d connection objects assigned for deletion from 'Connection' table", [connectionsToDelete count]);
        [objectsToDelete addObjectsFromArray:connectionsToDelete];
    }
    
    if(codeNameToDelete != nil){
        NSLog(@"Retrieved code name assigned for deletion from 'CodeName' table");
        [objectsToDelete addObject:codeNameToDelete];
    }
    
    
    NSLog(@"Total number of objects assigned for deletion: %d", [objectsToDelete count]);
    
    [PFObject deleteAllInBackground:objectsToDelete block:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            NSLog(@"CodeName deletion from all tables was a success!");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSLog(@"Could not delete codename object and references: Error: %@",error);
        }
    }];
}

#pragma mark - Editing Methods
- (IBAction)editCodeName:(UIBarButtonItem *)sender {
    NSLog(@"User pressed 'Edit' button. Entered editCodeName method");
    if([self isEditing]){
        NSLog(@"Turning edit mode off");
        UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonSystemItemDone target:self action:@selector(editCodeName:)];
        self.navigationItem.rightBarButtonItem = newButton;
        _editButton = newButton;
        [self updateCodeName];
        self.isEditing = NO;
    }else{
        NSLog(@"Turning edit mode on");
        UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonSystemItemEdit target:self action:@selector(editCodeName:)];
        self.navigationItem.rightBarButtonItem = newButton;
        _editButton = newButton;
        self.isEditing = YES;
        [self showAllFieldsForEditing];
    }
}

- (void)updateCodeName {
    NSLog(@"Entered updateCodeName method");
    PFQuery *query = [PFQuery queryWithClassName:@"CodeName"];
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    // Retrieve the object by id
    NSString *codenameId = self.codeName.parseObjectId;
    NSLog(@"object id for parse codename: %@",codenameId);
    
    [query getObjectInBackgroundWithId:codenameId block:^(PFObject *codename, NSError *error) {
        if(codename != nil){
            NSLog(@"Retrieved codename from query");
        }
        if([self.nameTextField.text isEqualToString:@""]){
            NSLog(@"Error editing private contact -- code name cannot be blank");
            UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Codename Error"
                                                                   message:@"Codename cannot be blank"
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertsuccess show];

        }else if ([self isCodeNameAlreadyPresent:self.nameTextField.text : self.descriptionTextView.text]) {
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Duplicate" message:@"You already have this code name" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
                
            } else {

                codename[@"codeName"] = self.nameTextField.text;
                codename[@"description"] = (self.descriptionTextView.text) ? self.descriptionTextView.text : [NSNull null];
                
                [codename saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if(succeeded){
                        NSLog(@"Successfully edited codename");
                        self.codeName = [Helper convertPFObjectToCodeName:codename];
                        [self populateFieldsAndHideEmptyCells];
                    }else{
                        NSLog(@"Error editing private contact");
                        UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Codename Error"
                                                                               message:@"Codename was not able to be edited"
                                                                              delegate:self
                                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alertsuccess show];
                    }
                }];

        }
    }];
    
}

#pragma mark - Hide/Show Table Cells

- (void)populateFieldsAndHideEmptyCells
{
    
    self.hideSectionsWithHiddenRows = YES;
    self.nameTextField.text = [self.codeName name];
    [self.nameTextField setEnabled:NO];
    
    if(self.codeName.codeNamedescription == nil){
        self.descriptionTextView.text = @"";
    }else{
        self.descriptionTextView.text = [NSString stringWithFormat:@"%@",self.codeName.codeNamedescription];
        [self.descriptionTextView setEditable:NO];
    }
    [self cell:self.deleteCodenameCell setHidden:YES]; // Only show on Edit
    
    [self reloadDataAnimated:YES];
    
}

- (void) showAllFieldsForEditing
{
    NSLog(@"showAllFieldsForEditing method entered");
    self.hideSectionsWithHiddenRows = NO;
    
    
    [self cell:self.deleteCodenameCell setHidden:NO];
    [self.nameTextField setEnabled:YES];
    [self.descriptionTextView setEditable:YES];
    
    [self reloadDataAnimated:YES];
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark - helper methods

- (BOOL)isCodeNameAlreadyPresent:(NSString *)codename: (NSString *)description {
    for (CodeName *cn in self.codeNames) {
        if ([cn.name isEqualToString:codename] &&
            ( (cn.codeNamedescription == nil && description == nil)
             || [cn.codeNamedescription isEqualToString:description]) ) {
                return YES;
            }
    }
    return NO;
}


@end
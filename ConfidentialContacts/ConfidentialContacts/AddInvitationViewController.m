//
//  AddInvitationViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/12/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "AddInvitationViewController.h"

@interface AddInvitationViewController ()

@end

@implementation AddInvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [Helper makeTextViewRounded:self.selectedCodeNameDescription];

}

- (void)viewWillAppear:(BOOL)animated {
    [Helper changePrivateNavigationBarTitle:self:@"Add Invitation"];
    [super viewWillAppear:animated];
    // Add this here to reload Codenames every time the view is loaded
    [self getCodeNamesFromParseForLoggedInUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    NSLog(@"number of code name rows: %lu", (unsigned long)[self.codeNames count]);
    
    return [self.codeNames count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    CodeName *cn = [self.codeNames objectAtIndex:row];
    
    return cn.name;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
   
    CodeName *cn = [self.codeNames objectAtIndex:row];
    NSLog(@"Selected CodeName: %@", [cn name]);
    [self.selectedCodeNameDescription setText:[cn codeNamedescription]];
    self.selectedCodeName = cn;
}

#pragma mark - Retrieve codenames from user

// Retrieve all code names for the user, and store them in
- (void)getCodeNamesFromParseForLoggedInUser {
    [self.codeNamePickerView setDelegate:self];
    [self.codeNamePickerView setDataSource:self];
    [self.selectedCodeNameDescription setText:[[self.codeNames firstObject] codeNamedescription]];
    [self setInitialSelectedCodeName];

    NSLog(@"%@", self.codeNames);
}

/**
 If the user does not scroll in the uipickerview, the first code name needs to be selected. So automatically select it from the beginning.
 */
- (void)setInitialSelectedCodeName {
    
    if ([self.codeNames count] > 0) {
        self.selectedCodeName = [self.codeNames firstObject];
    }
}

// Check to see if all fields are filled out, if so see if invitee exists, if so check to see if there is an invitation pending, if not then save a new connection
- (IBAction)inviteUser:(id)sender {
    NSLog(@"Entered inviteUser method");
    
    if (![self.inviteeEmailAddress.text isEqual:@""] && self.selectedCodeName != nil) {
        NSLog(@"All fields filled out");
        
        // Check to see if user is trying to invite themselves
        NSString *currentUserEmail = [PFUser currentUser].email;
        
        if ([currentUserEmail isEqualToString:self.inviteeEmailAddress.text]) {
            NSLog(@"Logged in user is attempting to invite themselves.");
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Can't Connect To Yourself" message:@"You cannot enter the same email address as yourself." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
        } else {
            // Check to see if invitee exists
            PFQuery *query = [PFUser query];
            [query whereKey:@"email" equalTo:self.inviteeEmailAddress.text];
            
            
            [query getFirstObjectInBackgroundWithBlock:^(id inviteeUser, NSError *error) {
                if (!error) {
                    // If Invitee user exists, check to see if there is already a pending connection
                    NSLog(@"Invitee exists!");
                    PFObject *codeNameInParse = [self retrieveCodeName:[self.selectedCodeName parseObjectId]];
                    
                    if ([self doesInvitationAlreadyExist:(PFUser *)inviteeUser : codeNameInParse]) {
                        NSString *alertText = [NSString stringWithFormat:@"You have already invited %@ with your code name %@",
                                               self.inviteeEmailAddress.text, self.selectedCodeName.name];
                        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Already invited" message:alertText delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                        [alertView show];
                        self.inviteeEmailAddress.text = @"";
                        
                    } else {
                        //GOOD TO GO!
                        [self saveConnection:(PFUser *)inviteeUser :codeNameInParse];
                    }
                } else {
                    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Could not find user" message:@"Could not find user by provided email" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                    [alertView show];
                }
            }];
        }
    } else {
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Fill out all fields" message:@"Make sure you fill out the email address for the invitee and the code name you would like to use" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
    }
    
}

/// Checking to see if invitation already exists in database -- no need to add invitation twice
- (BOOL)doesInvitationAlreadyExist :(PFUser *)inviteeUser :(PFObject *)inviterCodeName {
    
    PFQuery *query = [PFQuery queryWithClassName:@"Connection"];
    [query whereKey:@"Inviter" equalTo:inviterCodeName];
    [query whereKey:@"inviteeUser" equalTo:inviteeUser];
    
    int num = [query countObjects];
    
    return (num > 0) ? YES : NO;
}

- (IBAction)backToInvitations:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (PFObject *)retrieveCodeName:(NSString *)objectId {
    NSLog(@"Entered retrieveCodeName method");
    PFQuery *query = [PFQuery queryWithClassName:@"CodeName"];
    [query whereKey:@"objectId" equalTo:objectId];
    
    return (PFObject *)[query getFirstObject];
}

- (void)saveConnection:(PFUser *)inviteeUser :(PFObject *)inviterCodeName {
    
    NSLog(@"entered saveConnection method");
    PFObject *connection = [PFObject objectWithClassName:@"Connection"];
    PFUser *currentUser = [PFUser currentUser];

    [connection setObject:[NSNumber numberWithBool:NO] forKey:@"Active"];
    [connection setObject:inviterCodeName forKey:@"Inviter"];
    [connection setObject:inviteeUser forKey:@"inviteeUser"];
    [connection setObject:[inviterCodeName objectId] forKey:@"inviterObjectId"];
    
    PFACL *groupACL = [PFACL ACL];
    [groupACL setReadAccess:YES forUser:currentUser];
    [groupACL setWriteAccess:YES forUser:currentUser];
    [groupACL setReadAccess:YES forUser:inviteeUser];
    [groupACL setWriteAccess:YES forUser:inviteeUser];
    
    [connection setACL:groupACL];
    
    [connection saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            NSLog(@"Successfully invited %@", [inviteeUser email]);
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Successfull invite" message:@"Sent Connection Invitation" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                NSLog(@"Error inviting %@. Error: %@", [inviteeUser email], error);
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error during invite" message:@"Could not invite user." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
            }
        }];
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

@end
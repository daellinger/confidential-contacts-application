//
//  Helper.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/15/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "Helper.h"
#import "LoginRegisterViewController.h"

@implementation Helper

#pragma mark - Helper Methods to Convert

+ (CodeName *)convertPFObjectToCodeName:(PFObject *)personFromParse {
    CodeName *cn = [[CodeName alloc]init];
    
    if ([personFromParse objectForKey:@"codeName"]) {
        cn.name = [personFromParse objectForKey:@"codeName"];
    }
    
    if ([personFromParse objectForKey:@"description"]) {
        cn.codeNamedescription = [personFromParse objectForKey:@"description"];
    }
    cn.parseObjectId = [personFromParse objectId];
    
    return cn;
}

+ (Person *)convertPFObjectToPerson:(PFObject *)personFromParse {
    Person *person = [[Person alloc] init];
    
    if ([personFromParse objectForKey:@"firstName"]) {
        person.firstName = [personFromParse objectForKey:@"firstName"];
    }
    
    if ([personFromParse objectForKey:@"lastName"]) {
        person.lastName = [personFromParse objectForKey:@"lastName"];
    }
    
    if ([personFromParse objectForKey:@"companyName"]) {
        person.companyName = [personFromParse objectForKey:@"companyName"];
    }
    
    if ([personFromParse objectForKey:@"workEmail"]) {
        person.workEmail = [personFromParse objectForKey:@"workEmail"];
    }
    
    if ([personFromParse objectForKey:@"homeEmail"]) {
        person.homeEmail = [personFromParse objectForKey:@"homeEmail"];
    }
    
    if ([personFromParse objectForKey:@"homePhone"]) {
        person.homePhone = [personFromParse objectForKey:@"homePhone"];
    }
    
    if ([personFromParse objectForKey:@"workPhone"]) {
        person.workPhone = [personFromParse objectForKey:@"workPhone"];
    }
    
    if ([personFromParse objectForKey:@"cellPhone"]) {
        person.cellPhone = [personFromParse objectForKey:@"cellPhone"];
    }
    
    person.parseObjectId = [personFromParse objectId];
    
    return person;
}

+ (void)userLogout {
    [PFUser logOut];
    NSLog(@"User has been logged out");
}

+ (void)logOutAndresetAppToPublicMessagingViewController:(UIWindow *)window {
    
    NSLog(@"Entered logOutAndresetAppToPublicMessagingViewController method, also logging out of Parse");
    [self userLogout];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *myVC = (UITabBarController *)[storyboard instantiateViewControllerWithIdentifier:@"publicMessagingStoryboardTabBar"];
    [window setRootViewController:myVC];
}

+ (void)logOutAndresetAppToHome:(UIWindow *)window {
    
    NSLog(@"Entered logOutAndresetAppToPublicMessagingViewController method, also logging out of Parse");
    [self userLogout];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginRegisterViewController *myVC = (LoginRegisterViewController *)[storyboard instantiateInitialViewController];
    [window setRootViewController:myVC];
    
}

#pragma mark - Notification Check Methods

+ (void)checkToSeeIfThereAreAnyPendingConnections:(UITabBarController *)tabBarController {
    
    NSLog(@"checkToSeeIfThereAreAnyPendingConnections");
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    [postQuery whereKey:@"inviteeUser" equalTo:[PFUser currentUser]];
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:NO]];
    [postQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        NSLog(@"There are %d pending connections", number);
        
        if (number > 0) {
            [[tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%d", number]];
        } else {
            [[tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
        }
    }];
    
}

+ (void)makeTextViewRounded:(UITextView *)textView {
    [[textView layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[textView layer] setBorderWidth:2.3];
    [[textView layer] setCornerRadius:15];
}

#pragma mark - navigation bar methods

+ (void)changePrivateNavigationBarTitle:(UIViewController *)vc:(NSString *) navBarTitleText {
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor redColor], NSForegroundColorAttributeName,
                                    [UIColor redColor], NSBackgroundColorAttributeName,nil];
    vc.navigationController.navigationBar.titleTextAttributes = textAttributes;
    vc.title = navBarTitleText;
    
}

/**
 Query Parse for all Active connections. All Messages are a relation from each connection.
 */
+ (NSArray *)getAllConnectionsForLoggedInUser {
    
    NSLog(@"entered getMessagesForLoggedInUser");
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:YES]];
    [postQuery includeKey:@"Inviter"];
    [postQuery includeKey:@"Invitee"];
    [postQuery includeKey:@"Inviter.user"];
    [postQuery includeKey:@"Invitee.user"];
    
    return [postQuery findObjects];
}

+ (NSArray *)getAllMessagesForLoggedInUser:(PFObject *)connection {
    PFRelation *messagesRelation = [connection relationForKey:@"messages"];
    PFQuery *latestMessageQuery = [messagesRelation query];
    [latestMessageQuery includeKey:@"from"];
    [latestMessageQuery includeKey:@"to"];
    [latestMessageQuery addAscendingOrder:@"createdAt"];
    NSArray *temp = [[NSArray alloc] initWithArray:[latestMessageQuery findObjects]];
    NSLog(@"Retrieved %d messages from Parse", [temp count]);
    return temp;
}

+ (NSMutableDictionary *)generateDictionaryOfConnectionKeysWithAssociatedMessages {
    NSArray *connections = [self getAllConnectionsForLoggedInUser];
    NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
    
    for (PFObject *connection in connections) {
        NSArray *messages = [self getAllMessagesForLoggedInUser:connection];
        [temp setValue:messages forKey:[connection objectId]];
    }
    
    return temp;
}

/**
 Used to create a push channel -- used in pushing messages to users
 */
+ (void)subscribeUserToTheirChannel:(NSString *)userObjectId{
    NSString *channel = [[NSString alloc]initWithFormat:@"a%@", userObjectId]; // channel must start with a letter, just appending 'a' to the channel name
    NSLog(@"Subscribing user to %@ channel", channel);
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation addUniqueObject:channel forKey:@"channels"];
    [currentInstallation save];
}

/**
 Used to unsubscribe a push channel -- used in pushing messages to users
 */
+ (void)unsubscribeUserFromTheirChannel:(NSString *)userObjectId {
    NSString *channel = [[NSString alloc]initWithFormat:@"%@", userObjectId];
    NSLog(@"Unsubscribing user from %@ channel", channel);
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation removeObject:channel forKey:@"channels"];
    [currentInstallation saveInBackground];
}

+ (void)pulldownMenuStyles:(PulldownMenu *)menu {

    menu.cellHeight = 45.0;
    menu.handleHeight = 0.0;
    menu.topMarginPortrait = 45.0;
    menu.cellColor = [UIColor darkGrayColor];
    menu.cellSelectedColor = [UIColor clearColor];
    menu.cellSelectionStyle = UITableViewCellSelectionStyleNone;
    menu.backgroundColor = [UIColor darkGrayColor];
    menu.cellFont = [UIFont fontWithName:@"GillSans-Bold" size:16.0f];
    
}

@end
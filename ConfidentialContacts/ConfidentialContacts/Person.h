//
//  Person.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/2/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//
//  Holds information pertaining to a Person Object

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *homeEmail;
@property (nonatomic, strong) NSString *workEmail;
@property (nonatomic, strong) NSMutableArray *email;


@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSString *homePhone;
@property (nonatomic, strong) NSString *workPhone;
@property (nonatomic, strong) NSString *cellPhone;
@property (nonatomic, strong) NSMutableArray  *phone;

@property (nonatomic, strong) NSString *sortByName; // Used only for sorting

@property (nonatomic) NSString *parseObjectId;

- (NSComparisonResult)compareFirstName:(Person *)otherObject;

@end

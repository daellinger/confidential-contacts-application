//
//  AddContactViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/11/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <Parse/Parse.h>
#import "Person.h"
#import "DTAlertView.h"
#import "Helper.h"

@interface AddContactViewController : UITableViewController <UITextFieldDelegate, UITableViewDelegate>

// Text Fields To Add Public contact
@property (strong, nonatomic) IBOutlet UITextField *firstNameToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *homeEmailToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *workEmailToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *companyNameToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *homePhoneToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *workPhoneToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *cellPhoneToSaveTextField;

@property (strong, nonatomic) NSArray *alreadySavedContacts;

- (IBAction)backToContacts:(UIBarButtonItem *)sender;
- (IBAction)savePrivateContact:(UIBarButtonItem *)sender;

@end

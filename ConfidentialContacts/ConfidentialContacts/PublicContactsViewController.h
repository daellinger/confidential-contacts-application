//
//  PublicContactsViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/24/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//
//  Displays the device contacts

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <iAd/iAd.h>
#import "SingleContactViewController.h"
#import "Person.h"
#import "Helper.h"


@interface PublicContactsViewController : UIViewController <ADBannerViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) IBOutlet ADBannerView *banner;
@property (strong, nonatomic) NSMutableArray *filteredContactArray;
@property IBOutlet UISearchBar *contactSearchBar;

- (void)addContactsIntoTableData:(RHAddressBook *) addressBook;

@end

//
//  SingleContactViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/2/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "SingleContactViewController.h"
#import "Person.h"

@interface SingleContactViewController ()

//@property (strong, nonatomic) IBOutlet ADBannerView *banner;
@property (atomic, assign) BOOL isEditing;

- (IBAction)popBackToContacts:(UIBarButtonItem *)sender;
- (IBAction)editContact:(UIBarButtonItem *)sender;

@end

@implementation SingleContactViewController

#pragma mark - Lifecycle methods

- (void)viewWillAppear:(BOOL)animated {
    [self populateFieldsAndHideEmptyCells];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    //Disable the highlight effect of clicking a table row
    [self.firstNameCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.lastNameCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.homeEmailCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.workEmailCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.companyNameCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.homePhoneNumberCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.workPhoneNumberCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.cellPhoneNumberCell setSelectionStyle:UITableViewCellEditingStyleNone];
    [self.deleteContactCell setSelectionStyle:UITableViewCellEditingStyleNone];


    [self.tableView setBackgroundColor:[UIColor lightGrayColor]];


}

#pragma mark - Editing Methods

- (IBAction)editContact:(UIBarButtonItem *)sender {
    NSLog(@"User pressed 'Edit' button. Entered editContact method");
    if ([self isEditing]) {
        NSLog(@"Turning edit more off");
        UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonSystemItemDone target:self action:@selector(editContact:)];
        self.navigationItem.rightBarButtonItem = newButton;
        _editButton = newButton;
        [self updatePrivateContact];
        self.isEditing = NO;
    } else {
        NSLog(@"Turning edit mode on");
        UIBarButtonItem *newButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonSystemItemEdit target:self action:@selector(editContact:)];
        self.navigationItem.rightBarButtonItem = newButton;
        _editButton = newButton;
        self.isEditing = YES;
        [self showAllFieldsForEditing];
    }
}

#pragma mark - Hide/Show Table Cells

- (void)populateFieldsAndHideEmptyCells {
    
    self.hideSectionsWithHiddenRows = YES;
    
    if (self.person.firstName == nil || [self.person.firstName length] == 0) {
        NSLog(@"firstName not present. Hide Cell");
        [self cell:self.firstNameCell setHidden:YES];
    } else {
        self.firstNameTextField.text = [self.person firstName];
        [self.firstNameTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.firstNameTextField setEnabled:NO];
    }
    
    if (self.person.lastName == nil || [self.person.lastName length] == 0) {
        NSLog(@"lastName not present. Hide Cell");
        [self cell:self.lastNameCell setHidden:YES];
    } else {
        self.lastNameTextField.text = [self.person lastName];
        [self.lastNameTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.lastNameTextField setEnabled:NO];
    }
    
    if (self.person.companyName == nil || [self.person.companyName length] == 0) {
        [self cell:self.companyNameCell setHidden:YES];
    } else {
        self.companyNameTextField.text = [self.person companyName];
        [self.companyNameTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.companyNameTextField setEnabled:NO];
    }
    
    if (self.person.homeEmail == nil || [self.person.homeEmail length] == 0) {
        [self cell:self.homeEmailCell setHidden:YES];
    } else {
        self.homeEmailTextField.text = [self.person homeEmail];
        [self.homeEmailTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.homeEmailTextField setEnabled:NO];
    }
    
    NSLog(@"person.workemail: %@", [self.person workEmail]);
    
    if (self.person.workEmail == nil || [self.person.workEmail length] == 0) {
        [self cell:self.workEmailCell setHidden:YES];
    } else {
        self.workEmailTextField.text = [self.person workEmail];
        [self.workEmailTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.workEmailTextField setEnabled:NO];
    }
    
    if (self.person.homePhone == nil || [self.person.homePhone length] == 0) {
        NSLog(@"home phone field not present. Hide Cell");
        [self cell:self.homePhoneNumberCell setHidden:YES];
    } else {
        self.homePhoneNumberTextField.text = [self.person homePhone];
        [self.homePhoneNumberTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.homePhoneNumberTextField setEnabled:NO];
    }
    
    if (self.person.workPhone == nil || [self.person.workPhone length] == 0) {
        NSLog(@"work phone field not present. Hide Cell");
        [self cell:self.workPhoneNumberCell setHidden:YES];
    } else {
        self.workPhoneNumberTextField.text = [self.person workPhone];
        NSLog(@"workPhone: %@", self.workPhoneNumberTextField.text);
        [self.workPhoneNumberTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.workPhoneNumberTextField setEnabled:NO];
    }
    
    if (self.person.cellPhone == nil || [self.person.cellPhone length] == 0) {
        NSLog(@"cell phone field not present. Hide Cell");
        [self cell:self.cellPhoneNumberCell setHidden:YES];
    } else {
        self.cellPhoneNumberTextField.text = [self.person cellPhone];
        [self.cellPhoneNumberTextField setClearButtonMode:UITextFieldViewModeNever];
        [self.cellPhoneNumberTextField setEnabled:NO];
    }
    
    [self cell:self.deleteContactCell setHidden:YES]; // Only show on Edit
    
    [self reloadDataAnimated:YES];
}

- (void)showAllFieldsForEditing {
    NSLog(@"showAllFieldsForEditing method entered");
    self.hideSectionsWithHiddenRows = NO;

    [self cell:self.firstNameCell setHidden:NO];
    [self cell:self.lastNameCell setHidden:NO];
    [self cell:self.companyNameCell setHidden:NO];
    [self cell:self.homeEmailCell setHidden:NO];
    [self cell:self.workEmailCell setHidden:NO];
    [self cell:self.homePhoneNumberCell setHidden:NO];
    [self cell:self.workPhoneNumberCell setHidden:NO];
    [self cell:self.cellPhoneNumberCell setHidden:NO];
    [self cell:self.deleteContactCell setHidden:NO];
    
    [self.firstNameTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.lastNameTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.companyNameTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.homeEmailTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.workEmailTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.homePhoneNumberTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.workPhoneNumberTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    [self.cellPhoneNumberTextField setClearButtonMode:UITextFieldViewModeUnlessEditing];
    
    [self.firstNameTextField setEnabled:YES];
    [self.lastNameTextField setEnabled:YES];
    [self.companyNameTextField setEnabled:YES];
    [self.workEmailTextField setEnabled:YES];
    [self.homeEmailTextField setEnabled:YES];
    [self.homePhoneNumberTextField setEnabled:YES];
    [self.workPhoneNumberTextField setEnabled:YES];
    [self.cellPhoneNumberTextField setEnabled:YES];
    
    [self reloadDataAnimated:YES];
}

#pragma mark - Navigation Methods

- (IBAction)popBackToContacts:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View Methods

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

#pragma mark - Update Edit Data

- (void)updatePrivateContact {
    PFQuery *query = [PFQuery queryWithClassName:@"Contact"];
    
    // Retrieve the object by id
    NSString *contactId = self.person.parseObjectId;
    NSLog(@"object id for parse contact: %@", contactId);
    query.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [query getObjectInBackgroundWithId:contactId block:^(PFObject *contact, NSError *error) {
        if (contact != nil) {
            NSLog(@"Retrieved contact from query");
        }
        
        contact[@"firstName"] = (self.firstNameTextField.text) ? self.firstNameTextField.text : [NSNull null];
        contact[@"lastName"] = (self.lastNameTextField.text) ? self.lastNameTextField.text : [NSNull null];
        contact[@"companyName"] = (self.companyNameTextField.text) ? self.companyNameTextField.text : [NSNull null];
        contact[@"homeEmail"] = (self.homeEmailTextField.text) ? self.homeEmailTextField.text : [NSNull null];
        contact[@"workEmail"] = (self.workEmailTextField.text) ? self.workEmailTextField.text : [NSNull null];
        contact[@"homePhone"] = (self.homePhoneNumberTextField.text) ? self.homePhoneNumberTextField.text : [NSNull null];
        contact[@"workPhone"] = (self.workPhoneNumberTextField.text) ? self.workPhoneNumberTextField.text : [NSNull null];
        contact[@"cellPhone"] = (self.cellPhoneNumberTextField.text) ? self.cellPhoneNumberTextField.text : [NSNull null];
        
        [contact setACL:[PFACL ACLWithUser:[PFUser currentUser]]];
        
        [contact saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Successfully edited the private contact");
                self.person = [Helper convertPFObjectToPerson:contact];
                [self populateFieldsAndHideEmptyCells];
            } else {
                NSLog(@"Error editing private contact");
                UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Contact Error"
                                                                       message:@"Contact was not able to be edited"
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertsuccess show];
                
            }
        }];
        
    }];

}

#pragma mark - Helper Methods

- (BOOL)isAnyEmailPresent {
    
    if([self.person homeEmail] != nil || [self.person workEmail] != nil){
        return YES;
    }else{
        return NO;
    }
}

- (Person *)populatePersonToSave {
    Person *person = [[Person alloc] init];

    person.firstName = self.firstNameTextField.text;
    person.lastName = self.lastNameTextField.text;
    person.workEmail = self.workEmailTextField.text;
    person.homeEmail = self.homeEmailTextField.text;
    person.companyName = self.companyNameTextField.text;
    person.homePhone = self.homePhoneNumberTextField.text;
    person.workPhone = self.workPhoneNumberTextField.text;
    person.cellPhone = self.cellPhoneNumberTextField.text;
    
    return person;
}



// Adopted from: http://stackoverflow.com/questions/1246439/uitextfield-for-phone-number
#pragma mark - Phone Number Format Methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    // if it's the phone number textfield format it.
    
    if([self.homePhoneNumberTextField isEqual:textField] ||
       [self.workPhoneNumberTextField isEqual:textField] ||
       [self.cellPhoneNumberTextField isEqual:textField]) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
        
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7){
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    }else if(simpleNumber.length <= 10){   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    }
    return simpleNumber;
}

#pragma mark - Delete Contact Methods

- (IBAction)deleteContact:(UIButton *)sender {
    NSLog(@"Entered deleteContact method");

    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        // You can get button title of clicked button.
        NSLog(@"User clicked on %@", _alertView.clickedButtonTitle);
        if([_alertView.clickedButtonTitle isEqualToString:@"Yes, Delete"]){
            [self deletePrivateContact];
        }
        
    };
    DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Confirm Deletion"
                                                        message:@"Are you sure you want to delete this private contact?"
                                              cancelButtonTitle:@"No" positiveButtonTitle:@"Yes, Delete"];
    [alertView show];
}

- (void)deletePrivateContact {
    PFQuery *query = [PFQuery queryWithClassName:@"Contact"];
    
    // Retrieve the object by id
    NSString *contactId = self.person.parseObjectId;
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    NSLog(@"object id for parse contact: %@", contactId);
    
    [query getObjectInBackgroundWithId:contactId block:^(PFObject *contact, NSError *error) {
        
        if (contact != nil) {
            NSLog(@"Retrieved contact from query");
        }

        [contact deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if(succeeded){
                [[PFUser currentUser] incrementKey:@"remainingContacts" byAmount:[NSNumber numberWithInt:1]];
                NSLog(@"Private Contact deletion was a success!");
                [[PFUser currentUser] save];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                NSLog(@"Could not delete private contact: Error: %@",error);
            }
        }];
    }];

}

#pragma mark - Table View Methods

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Set the text color of our header/footer text.
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
    
    // Set the background color of our header/footer.
    header.contentView.backgroundColor = [UIColor lightGrayColor];
    
    // You can also do this to set the background color of our header/footer,
    //    but the gradients/other effects will be retained.
    // view.tintColor = [UIColor blackColor];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ([tableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
        return nil;
    } else {
        return [super tableView:tableView titleForHeaderInSection:section];
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

@end
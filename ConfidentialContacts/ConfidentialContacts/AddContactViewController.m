//
//  AddContactViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/11/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "AddContactViewController.h"

@interface AddContactViewController ()

@end

@implementation AddContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)backToContacts:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private contact methods

- (IBAction)savePrivateContact:(UIBarButtonItem *)sender {

    BOOL acceptContact = NO;
    // Create Contact
    PFObject *newContact = [PFObject objectWithClassName:@"Contact"];
    
    if ([self isFieldValid:self.firstNameToSaveTextField]) {
        acceptContact = YES;
        [newContact setObject:self.firstNameToSaveTextField.text forKey:@"firstName"];
    }
    
    if ([self isFieldValid:self.lastNameToSaveTextField]) {
        acceptContact = YES;
        [newContact setObject:self.lastNameToSaveTextField.text forKey:@"lastName"];
    }
    
    if ([self isFieldValid:self.companyNameToSaveTextField]) {
        [newContact setObject:self.companyNameToSaveTextField.text forKey:@"companyName"];
    }
    
    if ([self isFieldValid:self.homeEmailToSaveTextField]) {
        [newContact setObject:self.homeEmailToSaveTextField.text forKey:@"homeEmail"];
    }
    
    if ([self isFieldValid:self.workEmailToSaveTextField]) {
        [newContact setObject:self.workEmailToSaveTextField.text forKey:@"workEmail"];
    }
    
    if ([self isFieldValid:self.homePhoneToSaveTextField]) {
        [newContact setObject:self.homePhoneToSaveTextField.text forKey:@"homePhone"];
    }
    
    if ([self isFieldValid:self.workPhoneToSaveTextField]) {
        [newContact setObject:self.workPhoneToSaveTextField.text forKey:@"workPhone"];
    }
    
    if ([self isFieldValid:self.cellPhoneToSaveTextField]) {
        [newContact setObject:self.cellPhoneToSaveTextField.text forKey:@"cellPhone"];
    }
    
    
    // Create relationship
    if (acceptContact) {
        PFUser *currentUser = [PFUser currentUser];
        [newContact setObject:currentUser forKey:@"user"];
        [newContact setACL:[PFACL ACLWithUser:currentUser]];
        [[PFUser currentUser] incrementKey:@"remainingContacts" byAmount:[NSNumber numberWithInt:-1]];
        [newContact saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [[PFUser currentUser] save];
                NSLog(@"Successfully added a new private contact. ObjectID:%@", [currentUser objectId]);
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                NSLog(@"Error adding a new private contact");
                UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Contact Error"
                                                                       message:@"Contact was not able to be added"
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertsuccess show];
                
            }
        }];
  
    } else {
        NSLog(@"Error adding a private contact - Need to have firstname/lastname or companyname filled out");
        UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Contact Error"
                                                               message:(@"Contact was not able to be added. Make sure First Name or Last Name is filled out")
                                                              delegate:self
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertsuccess show];
    }
}

- (Person *)populatePersonToSave {
    Person *person = [[Person alloc] init];
    
    
    person.firstName = [self isFieldValid:self.firstNameToSaveTextField] ? self.firstNameToSaveTextField.text : nil;
    person.lastName = [self isFieldValid:self.lastNameToSaveTextField] ? self.lastNameToSaveTextField.text : nil;
    person.companyName = [self isFieldValid:self.companyNameToSaveTextField] ? self.companyNameToSaveTextField.text : nil;
    person.workEmail = [self isFieldValid:self.workEmailToSaveTextField] ? self.workEmailToSaveTextField.text : nil;
    person.homeEmail = [self isFieldValid:self.homeEmailToSaveTextField] ? self.homeEmailToSaveTextField.text : nil;
    person.homePhone = [self isFieldValid:self.homePhoneToSaveTextField] ? self.homePhoneToSaveTextField.text : nil;
    person.workPhone = [self isFieldValid:self.workPhoneToSaveTextField] ? self.workPhoneToSaveTextField.text : nil;
    person.cellPhone = [self isFieldValid:self.cellPhoneToSaveTextField] ? self.cellPhoneToSaveTextField.text : nil;
    
    return person;
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark - table view methods

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark - helper methods

- (BOOL)isFieldValid:(UITextField *)field {
    if ([field.text isEqualToString:@""]) {
        NSLog(@"isFieldValid returning NO");
        return NO;
    } else {
        NSLog(@"isFieldValid returning YES");
        
        return YES;
    }

}

#pragma mark - Phone Number Format Methods

// Adopted from: http://stackoverflow.com/questions/1246439/uitextfield-for-phone-number
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *totalString = [NSString stringWithFormat:@"%@%@", textField.text, string];
    
    // if it's the phone number textfield format it.
    if ([self.homePhoneToSaveTextField isEqual:textField] ||
       [self.workPhoneToSaveTextField isEqual:textField] ||
       [self.cellPhoneToSaveTextField isEqual:textField]) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    return YES; 
}

- (NSString *)formatPhoneNumber:(NSString *) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    if (deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if (simpleNumber.length < 7) {
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    } else if (simpleNumber.length <= 10) {   // else do this one..
    simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                           withString:@"($1) $2-$3"
                                                              options:NSRegularExpressionSearch
                                                                range:NSMakeRange(0, [simpleNumber length])];
    }
    return simpleNumber;
}

@end

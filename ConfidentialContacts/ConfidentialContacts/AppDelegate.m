//
//  AppDelegate.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/16/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "TestFlight.h"
#import "LoginRegisterViewController.h"
#import "Helper.h"
#import <Crashlytics.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [Crashlytics startWithAPIKey:@"a9b041a61023b0cb520989a498cf323317f47db5"];
    [Parse setApplicationId:@"NiKJX9ksVRWNtoG7EpFIVTI1PIEtDt1Q4O6E4Vqa"
                  clientKey:@"ozNQdlEnHMDIIcYFvKQECmq8ee8jGpcp6KmESXee"];
    
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    [TestFlight takeOff:@"20762d4c-fdc6-4227-a8db-9af7132bd8a5"];
    
    self.globalConnectionToMessagesDict = [[NSMutableDictionary alloc] init];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];

    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self resetAppToFirstController];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveAlertView" object:self]; // Fix bug that persisted the alert view when entering foreground
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"Registered Push Notifications");
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did fail to register for push, %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Entered didReceiveRemoteNotification method");
    [PFPush handlePush:userInfo];
    self.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];
}

- (void)resetAppToFirstController {
    NSLog(@"Entered resetAppToFirstController method, also logging out of Parse");
    [PFUser logOut];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginRegisterViewController *myVC = (LoginRegisterViewController *)[storyboard instantiateInitialViewController];
    [self.window setRootViewController:myVC];
}

@end

//
//  AddCodeNameViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "AddCodeNameViewController.h"

@interface AddCodeNameViewController ()

@end

@implementation AddCodeNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
    [Helper makeTextViewRounded:self.descriptionTextView];

    self.descriptionTextView.placeholder = [[NSString alloc]initWithFormat:@"Enter description for codename. The invitee will be able to view this description."];
}

- (void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)saveCodeName:(UIBarButtonItem *)sender {
    NSLog(@"entered saveCodeName method");
    
    if ([self isFieldValid:self.codenameTextField]) {
        
        NSString *description;
        if ([self isTextViewValid:self.descriptionTextView]) {
            description = self.descriptionTextView.text;
        }else{
            description = nil;
        }
        
        if ([self isCodeNameAlreadyPresent:self.codenameTextField.text:description]) {
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Duplicate" message:@"You already have this code name" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
            
        } else {
            PFObject *newCodename = [PFObject objectWithClassName:@"CodeName"];
            PFUser *currentUser = [PFUser currentUser];
            [newCodename setObject:currentUser forKey:@"user"];
            [newCodename setObject:self.codenameTextField.text forKey:@"codeName"];
            
            if (description != nil) {
                [newCodename setObject:self.descriptionTextView.text forKey:@"description"];
            }
            
            //TODO: Pass in code name array from previous view and check to see if the codename the user wants to enter is already present. Do not allow duplicates
            [newCodename saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"Successfully added a new codename: %@", self.codenameTextField.text);
                    [self.navigationController popViewControllerAnimated:YES];
                } else {
                    NSLog(@"Error adding a new code name");
                    UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Code Name Error"
                                                                           message:@"Code Name was not able to be added"
                                                                          delegate:self
                                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertsuccess show];
                    
                }
            }];
        }
            
    } else {
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Fill in Codename" message:@"Please fill in the codename field" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
        
    }
}

#pragma mark - helper methods

- (BOOL)isCodeNameAlreadyPresent:(NSString *)codename: (NSString *)description {
    for (CodeName *cn in self.codeNames) {
        if ([cn.name isEqualToString:codename] &&
            ( (cn.codeNamedescription == nil && description == nil)
             || [cn.codeNamedescription isEqualToString:description]) ) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isFieldValid:(UITextField *)field {
    
    if ([field.text isEqualToString:@""]) { 
        NSLog(@"isFieldValid returning NO");
        
        return NO;
    } else {
        NSLog(@"isFieldValid returning YES");
        
        return YES;
    }
    
}

- (BOOL)isTextViewValid:(UITextView *)field {
    
    if ([field.text isEqualToString:@""]) {
        NSLog(@"isTextViewValid returning NO");
        
        return NO;
    } else {
        NSLog(@"isTextViewValid returning YES");
        
        return YES;
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

@end
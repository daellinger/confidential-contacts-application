//
//  PrivateContactsViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/26/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//
//  Displays the users private contacts

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <iAd/iAd.h>
#import "SingleContactViewController.h"
#import "AddContactViewController.h"
#import "Person.h" 
#import "Helper.h"
#import "PulldownMenu.h"

@interface PrivateContactsViewController : UIViewController <ADBannerViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, UITabBarControllerDelegate, PulldownMenuDelegate, UIScrollViewDelegate>



@property (strong, nonatomic) IBOutlet ADBannerView *banner;
@property (strong, atomic) NSMutableArray *filteredContactArray;
@property IBOutlet UISearchBar *contactSearchBar;

- (IBAction)menuButtonPressed:(id)sender;

@end

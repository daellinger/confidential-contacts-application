//
//  PrivateMessagingViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/5/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "PrivateMessagingViewController.h"
#import "NSDate+NVTimeAgo.h"
#import "DetailedPrivateMessageViewController.h"
#import "AppDelegate.h"

@interface PrivateMessagingViewController ()
@property (strong, nonatomic) NSMutableArray *tableData;

//@property (strong, nonatomic) NSMutableDictionary *connectionObjects;
//@property (strong, nonatomic) NSMutableDictionary *connectionToMessages;
@property NSInteger rowIndex;

//Information that will be passed to detailed view
@property __block int allMessagesCount; //Running count of all messages for a user
@property (strong, nonatomic) PFObject *selectedConnection;
@property (strong, nonatomic) PFObject *selectedLoggedInUserCodeName;
@property (strong, nonatomic) PFObject *otherLoggedInUserCodeName;
@property (strong, nonatomic) NSString *otherUserPFObjectId; //Used for push messages
@property (strong, nonatomic) AppDelegate *delegate;

@property (strong, nonatomic) PulldownMenu *pulldownMenu;

@end

@implementation PrivateMessagingViewController

#pragma - Lifecycle methods

- (void)viewDidLoad {
    
    [super viewDidLoad];
    _banner.delegate = self;
    self.delegate = [UIApplication sharedApplication].delegate;
	// Do any additional setup after loading the view.
    [self setupPulldownMenu];
    self.delegate.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];
    [self.delegate.globalConnectionToMessagesDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        self.allMessagesCount = self.allMessagesCount + [(NSArray *)obj count];
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(reloadTable) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated {
	// Do any additional setup after loading the view.
    [Helper changePrivateNavigationBarTitle :self:@"Messages"];
    
    [super viewWillAppear:animated];
    [self getAllConnectionsForLoggedInUser];
    
    [self.messagesTableView reloadData];
    
}

#pragma mark - Timer Method

- (void)reloadTable {
    __block int count = 0;
    [self.delegate.globalConnectionToMessagesDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
       count = count + (int)[(NSArray *)obj count];
    }];
    
    if (self.allMessagesCount != count ) {
        NSLog(@"Timer Method");
        NSLog(@"Update the table, there is new data!");
        [self getAllConnectionsForLoggedInUser];
        self.delegate.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];
        [self.messagesTableView reloadData];
    }
}

/**
 Query Parse for all Active connections. All Messages are a relation from each connection.
 */
- (void)getAllConnectionsForLoggedInUser {
    
    NSLog(@"entered getMessagesForLoggedInUser");
    self.tableData = [[NSMutableArray alloc]init];
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:YES]];
    [postQuery includeKey:@"Inviter"];
    [postQuery includeKey:@"Invitee"];
    [postQuery includeKey:@"Inviter.user"];
    [postQuery includeKey:@"Invitee.user"];
    [self.tableData addObjectsFromArray:[postQuery findObjects]];
}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableData count];
}

- (MessageCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Entered cellForRowAtIndexPath");
    static NSString *cellIdentifier = @"Identifier";
    MessageCell *cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // Create a new PFObject Object
    PFObject *connection = nil;
    connection = [self.tableData objectAtIndex:[indexPath row]];
    
    //Pull code names from connections
    PFObject *inviterCodeName = connection[@"Inviter"];
    PFObject *inviteeCodeName = connection[@"Invitee"];
    
    PFUser *loggedInUser = [PFUser currentUser];

    // Pull the users from the codenames
    PFObject *inviterUser = inviterCodeName[@"user"];
    PFObject *inviteeUser = inviteeCodeName[@"user"];
    self.otherUserPFObjectId = [inviterUser objectId];
    // Messaging is only 2-way. Need to find out if the inviter/invitee code name is the logged in user or not.
    PFObject *loggedInUserCodeName;
    PFObject *otherUserCodeName;
    
    loggedInUserCodeName = ( [[inviterUser objectId] isEqualToString:[loggedInUser objectId]] ? inviterCodeName : inviteeCodeName);
    otherUserCodeName = ( [[inviteeUser objectId] isEqualToString:[loggedInUser objectId] ] ? inviterCodeName  : inviteeCodeName);
    
    NSArray *messages = [self.delegate.globalConnectionToMessagesDict valueForKey:[connection objectId]];
    PFObject *latestMessage = [messages lastObject];
    
    if ([messages count] > 0) {
        cell.dateLabel.text = [[latestMessage createdAt] formattedAsTimeAgo];
        cell.userCodeName.text = [NSString stringWithFormat:@"%@", loggedInUserCodeName[@"codeName"]];
        cell.userDescription.text = [NSString stringWithFormat:@"%@", loggedInUserCodeName[@"description"]];
        cell.otherCodeName.text = [NSString stringWithFormat:@"%@", otherUserCodeName[@"codeName"]];
        cell.otherDescription.text = [NSString stringWithFormat:@"%@", otherUserCodeName[@"description"]];
        cell.message.text = latestMessage[@"message"];
    } else {
        cell.dateLabel.text = @"";
        cell.userCodeName.text = [NSString stringWithFormat:@"%@", loggedInUserCodeName[@"codeName"]];
        cell.userDescription.text = [NSString stringWithFormat:@"%@", loggedInUserCodeName[@"description"]];
        cell.otherCodeName.text = [NSString stringWithFormat:@"%@", otherUserCodeName[@"codeName"]];
        cell.otherDescription.text = [NSString stringWithFormat:@"%@", otherUserCodeName[@"description"]];
        cell.message.text = @"";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell was clicked");
    self.rowIndex = indexPath.item;
    [self performSegueWithIdentifier:@"conversationSegue" sender:tableView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"entered prepareForSegue method");
    
    if ([segue.identifier isEqualToString:@"conversationSegue" ]) {
        NSLog(@"segue.identifier isEqualToString:@'conversationSegue'");
        
        DetailedPrivateMessageViewController *detailedMessageVC = segue.destinationViewController;
        NSLog(@"created DetailedPrivateMessageViewController");
        PFObject *connection = [self.tableData objectAtIndex:self.rowIndex];
        [detailedMessageVC setSelectedConnection:connection];
        [detailedMessageVC setOtherUserPFObjectId:self.otherUserPFObjectId];
        NSLog(@"Segue to conversationSegue");
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

#pragma mark - Menu Methods

- (void)setupPulldownMenu {
    self.pulldownMenu = [[PulldownMenu alloc] initWithView:self.view];
    [self.view addSubview:self.pulldownMenu];
    
    [Helper pulldownMenuStyles:self.pulldownMenu];
    
    [self.pulldownMenu insertButton:@"Show Remaining Messages"];
    [self.pulldownMenu insertButton:@"Purchase 1000 Messages"];
    [self.pulldownMenu insertButton:@"Purchase 2500 Messages"];
    [self.pulldownMenu insertButton:@"Purchase 5000 Messages"];
    [self.pulldownMenu insertButton:@"Logout"];
    
    self.pulldownMenu.delegate = self;
    
    [self.pulldownMenu loadMenu];
}

- (IBAction)menuButtonPressed:(id)sender {
    [self.pulldownMenu animateDropDown];
}

- (void)menuItemSelected:(NSIndexPath *)indexPath {
    
    switch (indexPath.item) {
        case 0:
            [self showRemainingMessages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 1:
            [self purchase1000Messages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 2:
            [self purchase2500Messages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 3:
            [self purchase5000Messages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 4:
            [self userLogOutandSegueLogic:nil];
            break;
        default:
            break;
    }
}

- (void)pullDownAnimated:(BOOL)open {
    
    if (open) {
        NSLog(@"Pull down menu open!");
    } else {
        NSLog(@"Pull down menu closed!");
    }
}

#pragma mark - show remaining messages

- (void)showRemainingMessages:(id)sender {
    NSNumber *remainingMessages = [[PFUser currentUser] objectForKey:@"remainingMessages"];
    
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Remaining Messages" message:[NSString stringWithFormat:@"You have %@ remaining messages. You will not be able to send messages if you get to zero.", remainingMessages] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    
    [alertView show];
}

#pragma mark - Purchase Messages

- (void)purchase1000Messages:(id)sender {
    NSLog(@"Entered purchase1000Messages method");

    [PFPurchase buyProduct:@"1000Messages" block:^(NSError *error) {
        if (error) {
            [self showPurchaseMessageError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@1000];
            NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                             message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                                                            delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
            
        }
    }];
}

- (void)purchase2500Messages:(id)sender {
    NSLog(@"Entered purchase2500Messages method");
    [PFPurchase buyProduct:@"2500Messages" block:^(NSError *error) {
    if (error) {
        [self showPurchaseMessageError:error];
    } else {
        [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@2500];
        NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
        [[PFUser currentUser] saveEventually];
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                         message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                                                        delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
        
    }
}];
}

- (void)purchase5000Messages:(id)sender {
    NSLog(@"Entered purchase5000Messages method");
       [PFPurchase buyProduct:@"5000Messages" block:^(NSError *error) {
        if (error) {
            [self showPurchaseMessageError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@5000];
            NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                             message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                                                            delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
            
        }
    }];
    
}

- (void)showPurchaseMessageError:(NSError *)error{
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error purchasing Messages" message:[NSString stringWithFormat:@"Could not purchase messages. Error: %@",[error description]] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    [alertView show];
}

#pragma mark - logout

- (void)userLogOutandSegueLogic:(id)sender {
    [Helper logOutAndresetAppToHome:self.view.window];
}



@end
//
//  InvitationCell.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/18/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse.h>
#import "InvitationAcceptDeclineButton.h"
#import "Helper.h"

@interface InvitationCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *codeName;
@property (strong, nonatomic) IBOutlet UITextView *codeNameDescription;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet InvitationAcceptDeclineButton *declineButton;
@property (strong, nonatomic) IBOutlet InvitationAcceptDeclineButton *acceptButton;

- (IBAction)declineInvitation:(id)sender;
- (IBAction)acceptInvitation:(id)sender;

@end
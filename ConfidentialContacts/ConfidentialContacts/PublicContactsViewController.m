//
//  PublicContactsViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/24/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "PublicContactsViewController.h"

@interface PublicContactsViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tableData;
@property NSInteger rowIndex;
@property RHAddressBook *ab;

@end

@implementation PublicContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _banner.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    self.tableData = [[NSMutableArray alloc]init];
    [self getPersonOutOfAddressBook];
    self.filteredContactArray = [NSMutableArray arrayWithCapacity:[self.tableData count]];
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"publicContactToSingleSegue" ]) {
        
        Person *person = nil;
        
        if (sender == self.searchDisplayController.searchResultsTableView) {
            if ([self.filteredContactArray count] == 0) {
                [self filterContentForSearchText:self.contactSearchBar.text scope:nil];
            }
            person = [self.filteredContactArray objectAtIndex:self.rowIndex];
        } else {
            person = [self.tableData objectAtIndex:self.rowIndex];
        }
        NSLog(@"prepareForSegue -- Person selected: %@", person.firstName);
        SingleContactViewController *singleContactViewController = segue.destinationViewController;
        [singleContactViewController setPerson:person];// create person object as a property in SingleContactViewController and assign the person object.
        NSLog(@"Segue to SingleContactViewController");
    } else if ([segue.identifier isEqualToString:@"publicContactsToAddSegue"]){
        NSLog(@"Going to publicContactsToAddSegue");
    }
    
}

#pragma mark - Address Book Methods

- (void)getPersonOutOfAddressBook {
    RHAddressBook *ab = [[RHAddressBook alloc] init];
    //query current status, pre iOS6 always returns Authorized
    if ( ([RHAddressBook authorizationStatus] == RHAuthorizationStatusNotDetermined) ||
         ([RHAddressBook authorizationStatus] == RHAuthorizationStatusDenied)) {
        //request authorization
        [ab requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
            NSLog(@"Authorized address book access!");
        }];
    }
    [self addContactsIntoTableData:ab];
    [self.tableView reloadData];
}

- (void)addContactsIntoTableData:(RHAddressBook *)ab {
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    NSLog(@"Retrieving device contacts");
    
    NSArray *allContacts = [ab peopleOrderedByFirstName];
    NSLog(@"%d public contacts are detected", [allContacts count]);
    for (RHPerson *p in allContacts) {
        
        Person *person = [[Person alloc] init];
        
        NSString *firstName = [p firstName];
        NSString *lastName =  [p lastName];
        NSString *fullName = [p name];
        NSString *companyName = [p organization];
        
        if (firstName) {
            person.firstName = firstName;
        }
        
        if (lastName) {
            person.lastName = lastName;
        }
        
        if (fullName) {
            person.fullName = fullName;
        }
        
        if (companyName) {
            person.companyName = companyName;
        }
        
        //email
        RHMultiValue *emailsMultiValue = [p emails];
        
        if (emailsMultiValue != nil) {
            int numOfEmails = [emailsMultiValue count];
            
            for (int i = 0; i < numOfEmails; i++) {
                NSString *label = [emailsMultiValue localizedLabelAtIndex:i];
                NSString *email = [emailsMultiValue valueAtIndex:i];
                
                if ([label isEqualToString:@"home"]) {
                    person.homeEmail = email;
                } else if ([label isEqualToString:@"work"]) {
                    person.workEmail = email;
                }
            }
        }
        
        //Phone
        RHMultiStringValue *phonesMultiValue = [p phoneNumbers];
        
        if (phonesMultiValue != nil) {
            int numOfNumbers = [phonesMultiValue count];
            
            for (int j = 0; j < numOfNumbers; j++) {
                NSString *label = [phonesMultiValue localizedLabelAtIndex:j];
                NSString *phoneNum = [phonesMultiValue valueAtIndex:j];
                
                if ( [label isEqualToString:@"home"]) {
                    person.homePhone = phoneNum;
                } else if ([label isEqualToString:@"work"]) {
                    person.workPhone = phoneNum;
                } else if ([label isEqualToString:@"mobile"]) {
                    person.cellPhone = phoneNum;
                }
            }
        }
 
        // Prevents "No Names"
        if ([self isFieldPresent:person.firstName] || [self isFieldPresent:person.lastName]) {
            NSLog(@"Person Email: %@ ---- Person Phone: %@", [person.email description], [person.phone description]);
            [temp addObject:person];
        }
        
    }
    [self.tableData addObjectsFromArray:temp];
    NSLog(@"%d public contacts have ACTUALLY been imported.", [self.tableData count]);
    
}

- (IBAction)backToPublicContacts:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Check to see whether the normal table or search results table is being displayed and return the count from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredContactArray count];
    } else {
        return [self.tableData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Identifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    // Create a new Person Object
    Person *person = nil;
    
    // Check to see whether the normal table or search results table is being displayed and set the Person object from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        person = [self.filteredContactArray objectAtIndex:[indexPath row]];
    } else {
        person = [self.tableData objectAtIndex:[indexPath row]];
    }
    
    if ([self isFieldPresent:person.firstName]) {
        
        if ([self isFieldPresent:person.lastName]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [person firstName],[person lastName]];
        } else {
            cell.textLabel.text = [NSString stringWithFormat:@"%@", [person firstName]];
        }
    } else if ([self isFieldPresent:person.lastName]) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@", [person lastName]];
    } else {
        cell.textLabel.text = @"No Name";
        cell.textLabel.font = [UIFont italicSystemFontOfSize:cell.textLabel.font.pointSize];
    }
    
    return cell;
}

- (bool)isFieldPresent : (NSString *)field {
    
    if ( (field == nil) || (field == NULL) || ([field isEqualToString:@"" ])) {
        return NO;
    } else {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     NSLog(@"Cell was clicked");
    self.rowIndex = indexPath.item; // performForSegue will happen before I can grab the index, so I need to initialize the index into this variable
    [self performSegueWithIdentifier:@"publicContactToSingleSegue" sender:tableView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}



#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return NO;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    controller.searchResultsTableView.backgroundColor = [UIColor lightGrayColor];
}

#pragma mark - Search Bar Methods

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredContactArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.firstName contains[c] %@) OR (SELF.lastName contains[c] %@)",searchText,searchText,searchText];
    self.filteredContactArray = [NSMutableArray arrayWithArray:[self.tableData filteredArrayUsingPredicate:predicate]];
}


@end

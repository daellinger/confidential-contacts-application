//
//  PrivateContactsViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/26/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "PrivateContactsViewController.h"
#import "AppDelegate.h"

@interface PrivateContactsViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *tableData;
@property (strong, nonatomic) NSMutableArray *parseContacts;
@property NSInteger rowIndex;
// Text Fields To Add Public contact
@property (strong, nonatomic) IBOutlet UITextField *firstNameToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *homeEmailToSaveTextField;
@property (strong, nonatomic) IBOutlet UITextField *workEmailToSaveTextField;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) PulldownMenu *pulldownMenu;

@end

@implementation PrivateContactsViewController

#pragma mark - Lifecycle methods

- (void)viewDidAppear:(BOOL)animated {
        [self segueToMainScreenIfUserIsLoggedOut];
        [self becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self segueToMainScreenIfUserIsLoggedOut];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
    [self setupPulldownMenu];
    self.tableData = [[NSMutableArray alloc]init];
    [Helper checkToSeeIfThereAreAnyPendingConnections:self.tabBarController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Helper changePrivateNavigationBarTitle :self:@"Contacts"];
    // Having this code here allows the AddContactView/SingleContactView to update a contact and having this here will
    // update the contact in the main table view instead of having the old data.
    [self getContactsFromParseForLoggedInUser];
    self.filteredContactArray = [[NSMutableArray alloc] init];
    
    [self.tableView reloadData];
    self.appDelegate.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];
}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"privateContactToSingleSegue"]) {
        //self.contactSearchBar.text = @"";
        Person *person = nil;
        
        if (sender == self.searchDisplayController.searchResultsTableView) {
            if ([self.filteredContactArray count] == 0) {
                [self filterContentForSearchText:self.contactSearchBar.text scope:nil];
            }
            person = [self.filteredContactArray objectAtIndex:self.rowIndex];
        } else {
            person = [self.tableData objectAtIndex:self.rowIndex];
        }
      
        NSLog(@"Person selected: %@", person.firstName);
        SingleContactViewController *singleContactViewController = segue.destinationViewController;
        [singleContactViewController setPerson:person];
        NSLog(@"created singlecontactviewcontroller");
        NSLog(@"Segue to privateContactToSingleSegue");
    } else if ([segue.identifier isEqualToString:@"addPrivateContactSegue"]) {
        NSLog(@"Segue to addPrivateContactSegue");
        AddContactViewController *addContactViewController = segue.destinationViewController;
        [addContactViewController setAlreadySavedContacts:self.tableData];
    }
    
}

#pragma mark - Retrieve contacts From user

- (void)getContactsFromParseForLoggedInUser {
    // Create a query, grab everything...due to ACL restrictions it will only grab the Users Contacts.
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Contact"];
    postQuery.limit = 1000;
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //NSLog(@"%@",objects);
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Person *person = [Helper convertPFObjectToPerson:obj];
                [temp addObject:person];
            }];
            self.tableData = [self sortArrayByKey :temp :@"firstName"];
            [self.tableView reloadData];
            NSLog(@"Loaded %lu contacts from Parse for logged in user", (unsigned long)[temp count]);
        } else {
            NSLog(@"Could not find any contacts from Parse");
        }
    }];
}

// keyToSortOn can be any Contact property
- (NSMutableArray *)sortArrayByKey :(NSMutableArray *)arrayToSort :(NSString *)keyToSortOn {
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:keyToSortOn
                                                 ascending:YES
                                                  selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    return [[arrayToSort sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
}

#pragma mark - Logout methods

- (void)segueToMainScreenIfUserIsLoggedOut {
    if ([PFUser currentUser] == nil) {
        [self performSegueWithIdentifier:@"privateToHomeSegue" sender:self];
    }
}

- (void)userLogOutandSegueLogic:(id)sender {
    [Helper logOutAndresetAppToHome:self.view.window];
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    } 
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[PrivateContactsViewController class]]) {
        [viewController canBecomeFirstResponder];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Check to see whether the normal table or search results table is being displayed and return the count from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredContactArray count];
    } else {
        return [self.tableData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Identifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Create a new Person Object
    Person *person = nil;
    
    // Check to see whether the normal table or search results table is being displayed and set the Person object from the appropriate array
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        person = [self.filteredContactArray objectAtIndex:[indexPath row]];
    } else {
        person = [self.tableData objectAtIndex:[indexPath row]];
    }
 
    // These length conditions are just checking if the values are populated, seemed to work best this way
    if ([person.firstName length]) {
        if ([person.lastName length]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", person.firstName, person.lastName];
        } else {
            cell.textLabel.text = [NSString stringWithFormat:@"%@", person.firstName];
        }
    } else if ([person.lastName length]) {
            cell.textLabel.text = [NSString stringWithFormat:@"%@", person.lastName];
    } else {
        cell.textLabel.text = @"No Name";
        cell.textLabel.font = [UIFont italicSystemFontOfSize:cell.textLabel.font.pointSize];

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell was clicked");
    self.rowIndex = indexPath.item; // performForSegue will happen before I can grab the index, so I need to initialize the index into this variable
    [self performSegueWithIdentifier:@"privateContactToSingleSegue" sender:tableView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    controller.searchResultsTableView.backgroundColor = [UIColor lightGrayColor];
}

#pragma mark - Search Bar Methods

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [self.filteredContactArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(SELF.firstName contains[c] %@)", searchText];
    self.filteredContactArray = [NSMutableArray arrayWithArray:[self.tableData filteredArrayUsingPredicate:predicate]];
}

#pragma mark - Menu Methods

- (IBAction)menuButtonPressed:(id)sender {
    [self.pulldownMenu animateDropDown];
}

- (void)setupPulldownMenu {
    self.pulldownMenu = [[PulldownMenu alloc] initWithView:self.view];
    [self.view addSubview:self.pulldownMenu];
    
    [Helper pulldownMenuStyles:self.pulldownMenu];
    
    [self.pulldownMenu insertButton:@"Show Remaining Contacts"];
    [self.pulldownMenu insertButton:@"Add Contact"];
    [self.pulldownMenu insertButton:@"Import Public Contacts"];
    [self.pulldownMenu insertButton:@"Delete all Private Contacts"];
    [self.pulldownMenu insertButton:@"Purchase 100 Private Contacts"];
    [self.pulldownMenu insertButton:@"Purchase 300 Private Contacts"];
    [self.pulldownMenu insertButton:@"Logout"];
    
    self.pulldownMenu.delegate = self;
    
    [self.pulldownMenu loadMenu];
}

- (void)menuItemSelected:(NSIndexPath *)indexPath {

    switch (indexPath.item) {
        case 0:
            [self showRemainingContacts:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 1:
            [self addContactSegue:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 2:
            [self importPublicContacts:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 3:
            [self confirmDeletePrivateContacts:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 4:
            [self purchase100Contacts:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 5:
            [self purchase300Contacts:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 6:
            [self userLogOutandSegueLogic:nil];
            break;
        default:
            break;
    }
}

- (void)pullDownAnimated:(BOOL)open {
    
    if (open) {
        NSLog(@"Pull down menu open!");
    } else {
        NSLog(@"Pull down menu closed!");
    }
}

#pragma mark - Purchase Contacts

- (void)purchase100Contacts:(id)sender {
    NSLog(@"Entered purchase100Contacts method");
    [PFPurchase buyProduct:@"100Contacts" block:^(NSError *error) {
        if (error) {
            [self showPurchaseContactsError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingContacts" byAmount:@100];
            NSLog(@"Current user has %@ remaining contacts left.", [[PFUser currentUser] objectForKey:@"remainingContacts"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                             message:[NSString stringWithFormat:@"You now have %d contacts available!",[[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue]]
                                                            delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
            
        }
    }];
}

- (void)purchase300Contacts:(id)sender {
    NSLog(@"Entered purchase300Contacts method");
    [PFPurchase buyProduct:@"300Contacts" block:^(NSError *error) {
        if (error) {
            [self showPurchaseContactsError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingContacts" byAmount:@300];
            NSLog(@"Current user has %@ remaining contacts left.", [[PFUser currentUser] objectForKey:@"remainingContacts"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                             message:[NSString stringWithFormat:@"You now have %d contacts available!",[[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue]]
                                                            delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
            
        }
    }];
    
    
}

- (void)showPurchaseContactsError:(NSError *)error{
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error purchasing Contacts" message:[NSString stringWithFormat:@"Could not purchase contacts. Error: %@",[error description]] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    [alertView show];
}


#pragma mark - show remaining contacts

- (void)showRemainingContacts:(id)sender {
    NSNumber *remainingContacts = [[PFUser currentUser] objectForKey:@"remainingContacts"];
    
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Remaining Contacts" message:[NSString stringWithFormat:@"You have %@ remaining contacts.", remainingContacts] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    
    [alertView show];
}

- (void)addContactSegue:(id)sender {
    NSLog(@"entered addContactSegue method");
    int remainingContacts = [[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue];
    NSLog(@"User has %d contacts left", remainingContacts);
    // Check to see if the user has enough messages
    if ( remainingContacts > 0) {
        [self performSegueWithIdentifier:@"addPrivateContactSegue" sender:self];

    } else {
        [self outOfContactsPrompt];
    }
}

- (void)outOfContactsPrompt {
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Out of Contacts" message:@"You do not have any contacts left. Go to the menu to purchase more." delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    [alertView show];
}

#pragma mark - Delete All Contacts

- (void)confirmDeletePrivateContacts:(id)sender {
    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        if ([_alertView.clickedButtonTitle isEqualToString:@"Yes"]) {
            [self deleteAllPrivatecontacts];
        }
    };
    DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Deletion Disclaimer"
                                                    message:[NSString stringWithFormat:@"Deleting all of your private contacts will remove them forever. Are you sure you want to delete all private contacts?"]
                                          cancelButtonTitle:@"No" positiveButtonTitle:@"Yes"];
    [alertView show];

}

- (void)deleteAllPrivatecontacts {
    // Create a query
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Contact"];
    postQuery.limit = 1000;
    postQuery.cachePolicy = kPFCachePolicyCacheThenNetwork;
    
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            //NSLog(@"%@",objects);
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [temp addObject:(PFObject *)obj];
            }];
            int remainingContacts = [[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue];
            NSNumber *numberOfContactsAfterDeletion = [NSNumber numberWithUnsignedInt:(remainingContacts + [temp count])];
            [PFObject deleteAllInBackground:temp block:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"Deleted all Private contacts");
                    NSLog(@"Number of contacts to delete is %d", [temp count]);
                    NSLog(@"Number of contacts left available before deletion was %d", remainingContacts);
                    NSLog(@"Number of contacts left available after deletion is %@", numberOfContactsAfterDeletion);
                    [[PFUser currentUser] setValue:numberOfContactsAfterDeletion forKey:@"remainingContacts"];
                    [[PFUser currentUser] save];
                    NSLog(@"Number of contacts now is %d", [[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue]);
                } else {
                    NSLog(@"Error deleting all private contacts");
                }
                [self getContactsFromParseForLoggedInUser];
                [self.tableView reloadData];
            }];
        } else {
            NSLog(@"Could not find any contacts from Parse");
        }
    }];
}

#pragma mark - Import Methods

- (void)importPublicContacts:(id)sender {

    CFErrorRef error = NULL;
    
     RHAddressBook *ab = [[RHAddressBook alloc] init];
    if (ab != nil) {
        NSLog(@"importPublicContacts - addressBook successfull!.");
        DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
            if ([_alertView.clickedButtonTitle isEqualToString:@"Yes"]) {
                if (([RHAddressBook authorizationStatus] == RHAuthorizationStatusNotDetermined) ||
                    ([RHAddressBook authorizationStatus] == RHAuthorizationStatusDenied)) {
                    //request authorization
                    [ab requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
                        NSLog(@"Authorized address book access!");
                    }];
                }

                [self retrieveAllPublicContactsAndSaveToParse:ab];
            }
        };
        DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Import Disclaimer"
                                                        message:[NSString stringWithFormat:@"Importing will access your iOS device contacts into your private contacts, even if they are already present. Are you sure you want to import?"]
                                              cancelButtonTitle:@"No" positiveButtonTitle:@"Yes"];
        [alertView show];

    }
}

- (void) retrieveAllPublicContactsAndSaveToParse:(RHAddressBook *) ab {
    NSLog(@"Retrieving device contacts");
    
    NSArray *allContacts = [ab peopleOrderedByFirstName];
    NSMutableArray *parseContacts = [[NSMutableArray alloc] init];
    NSLog(@"%d public contacts are detected", [allContacts count]);
    for (RHPerson *p in allContacts) {
        
        Person *person = [[Person alloc] init];
        
        NSString *firstName = [p firstName];
        NSString *lastName =  [p lastName];
        NSString *fullName = [p name];
        NSString *companyName = [p organization];
        
        if (firstName) {
            person.firstName = firstName;
        }
        
        if (lastName) {
            person.lastName = lastName;
        }
        
        if (fullName) {
            person.fullName = fullName;
        }
        
        if (companyName) {
            person.companyName = companyName;
        }
        
        //email
        RHMultiValue *emailsMultiValue = [p emails];
        
        if (emailsMultiValue != nil) {
            int numOfEmails = [emailsMultiValue count];
            
            for (int i = 0; i < numOfEmails; i++) {
                NSString *label = [emailsMultiValue localizedLabelAtIndex:i];
                NSString *email = [emailsMultiValue valueAtIndex:i];
                
                if ([label isEqualToString:@"home"]) {
                    person.homeEmail = email;
                } else if ([label isEqualToString:@"work"]) {
                    person.workEmail = email;
                }
            }
        }
        
        //Phone
        RHMultiStringValue *phonesMultiValue = [p phoneNumbers];
        
        if (phonesMultiValue != nil) {
            int numOfNumbers = [phonesMultiValue count];
            
            for (int j = 0; j < numOfNumbers; j++) {
                NSString *label = [phonesMultiValue localizedLabelAtIndex:j];
                NSString *phoneNum = [phonesMultiValue valueAtIndex:j];
                
                if ( [label isEqualToString:@"home"]) {
                    person.homePhone = phoneNum;
                } else if ([label isEqualToString:@"work"]) {
                    person.workPhone = phoneNum;
                } else if ([label isEqualToString:@"mobile"]) {
                    person.cellPhone = phoneNum;
                }
                
            }
        }
        
        // Prevents "No Names"
        if ([self isFieldPresent:person.firstName] || [self isFieldPresent:person.lastName]) {
            
            // Copy Person into Parse object and save
            PFObject *newContact = [PFObject objectWithClassName:@"Contact"];
            
            if (person.firstName && person.firstName != nil ) {
                [newContact setObject:person.firstName forKey:@"firstName"];
            }
            
            if (person.lastName && person.lastName != nil) {
                [newContact setObject:person.lastName forKey:@"lastName"];
            }
            
            if (person.companyName && person.companyName != nil) {
                [newContact setObject:person.companyName forKey:@"companyName"];
            }
            
            if (person.homeEmail && person.homeEmail != nil) {
                [newContact setObject:person.homeEmail forKey:@"homeEmail"];
            }
            
            if (person.workEmail && person.workEmail != nil) {
                [newContact setObject:person.workEmail forKey:@"workEmail"];
            }
            
            if (person.homePhone && person.homePhone != nil) {
                [newContact setObject:person.homePhone forKey:@"homePhone"];
            }
            
            if (person.workPhone && person.workPhone != nil) {
                [newContact setObject:person.workPhone forKey:@"workPhone"];
            }
            
            if (person.cellPhone && person.cellPhone != nil) {
                [newContact setObject:person.cellPhone forKey:@"cellPhone"];
                
            }
            
            PFUser *currentUser = [PFUser currentUser];
            [newContact setObject:currentUser forKey:@"user"];
            [newContact setACL:[PFACL ACLWithUser:[PFUser currentUser]]];
            [parseContacts addObject:newContact];

        }
    }
    
    int remainingContacts = [[[PFUser currentUser] objectForKey:@"remainingContacts"] intValue];
    
    NSLog(@"Remaining contacts is %d", remainingContacts);
    NSLog(@"Number of contacts to import is %d", [parseContacts count]);
    
    if (remainingContacts < [parseContacts count]) {
        NSString *message = [[NSString alloc] initWithFormat:@"You currently have %d remaining contacts left to add. There are %lu public contacts that you are trying to import. Please purchase more contacts to accomodate this import.", remainingContacts, (unsigned long)[parseContacts count]];
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Not enough contacts" message:message delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
    } else {
        int newRemainingContacts = remainingContacts - [parseContacts count];
        [[PFUser currentUser] setValue:[[NSNumber alloc] initWithInt:newRemainingContacts] forKey:@"remainingContacts"];
                [PFObject saveAllInBackground:parseContacts block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Successfully imported %d public contacts", [parseContacts count]);
                [[PFUser currentUser] save];
            } else {
                NSLog(@"Error: Could not import %d public contacts", [parseContacts count]);
            }
            [self getContactsFromParseForLoggedInUser];
            [self.tableView reloadData];
        }];

    }
    
}

- (bool)isFieldPresent : (NSString *)field {
    
    if ( (field == nil) || (field == NULL) || ([field isEqualToString:@"" ])) {
        return NO;
    } else {
        return YES;
    }
}

@end
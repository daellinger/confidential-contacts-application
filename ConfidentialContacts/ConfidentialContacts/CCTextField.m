//
//  CCTextField.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 4/5/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "CCTextField.h"

@implementation CCTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:) ||
        action == @selector(copy:) ||
        action == @selector(cut:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

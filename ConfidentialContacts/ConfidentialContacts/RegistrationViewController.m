//
//  RegistrationViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/24/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) DTAlertView *alertView;

@end

@implementation RegistrationViewController

const int DEFAULT_MESSAGES_FOR_USER = 500;
const int DEFAULT_CONTACTS_FOR_USER = 100;
NSString * const englishTermsAndConditionLocation = @"termsAndConditionsEnglish";
NSString * const spanishTermsAndConditionLocation = @"termsAndConditionsSpanish";


- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
    
    [self loadEnglishTermsAndConditions];
    
    // set logo
    UIImage *image = [UIImage imageNamed:@"privateLock.png"];
    [self.mainLogoImage setImage:image];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAlertView) name:@"RemoveAlertView"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Keyboard Methods

- (void)keyboardWillShow:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -115.0f;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

- (void)removeAlertView {
    NSLog(@"Received RemoveAlertView Notification from AppDelegate");
    [self.alertView dismiss];
}

#pragma mark - Load Terms and Conditions

- (void)loadEnglishTermsAndConditions {
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:englishTermsAndConditionLocation ofType:@"html"];
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.tocWebView loadHTMLString:htmlString baseURL:nil];
}

- (void)loadSpanishTermsAndConditions {
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:spanishTermsAndConditionLocation ofType:@"html"];
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.tocWebView loadHTMLString:htmlString baseURL:nil];
}

#pragma mark - Scene Transition from Registration Fields to Terms and Conditions

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"registerInformationToTocSegue"]) {
        RegistrationViewController *targetVC = (RegistrationViewController *)segue.destinationViewController;
        
        if (nil != targetVC) {
            NSLog(@"prepareForSegue method");
            // Set variables with field information
            NSLog(@"Passing private user fields to RegistrationViewController instance");
            targetVC.username = self.usernameField.text;
            targetVC.email = self.emailField.text;
            targetVC.password = self.passwordField.text;
        }
    }
}

- (void)advanceToTermsScene:(UIButton *)sender {
    if ([self areAllFieldsValid]) {
        if ([self arePasswordFieldsTheSame]) {
            [self performSegueWithIdentifier:@"registerInformationToTocSegue" sender:self];
        }
    } else {
        NSLog(@"RegistrationViewController: advanceToTermsScene -- not all fields have been filled out.");
        UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Fields not filled out"
                                                               message:@"Please fill out all fields for registration."
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
        [alertsuccess show];
    }
}

- (IBAction)switchTocLanguage:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        [self loadEnglishTermsAndConditions];
    } else if(sender.selectedSegmentIndex == 1) {
        [self loadSpanishTermsAndConditions];
    }
}

#pragma mark - Registration Logic

- (IBAction)acceptRegistration:(UIButton *)sender {
    
    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        
        if ([_alertView.clickedButtonTitle isEqualToString:@"Yes"]) {
            // perform registration
            // Fields are valid due to check being done in segue
            
            PFUser *user = [PFUser user];
            
            NSLog(@"acceptRegistration method: username = %@, email = %@, password = %@", [self username],[self email],[self password]);
            [user setUsername:[self username]];
            [user setEmail:[self email]];
            [user setPassword:[self password]];
            [user setObject:[NSNumber numberWithInt:DEFAULT_MESSAGES_FOR_USER] forKey:@"remainingMessages"];
            [user setObject:[NSNumber numberWithInt:DEFAULT_CONTACTS_FOR_USER] forKey:@"remainingContacts"];
            
            [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    NSLog(@"User: %@ has been registered", user.username);
                    NSLog(@"User has %@ remaining messages", [NSNumber numberWithInt:DEFAULT_MESSAGES_FOR_USER]);
                    NSLog(@"User has %@ remaining contacts", [NSNumber numberWithInt:DEFAULT_CONTACTS_FOR_USER]);
                    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Registration"
                                                                     message:@"Thank you. An email to complete your registration has been sent to the address provided. Please check your 'Spam' folders for the confirmation email, it should only take a few minutes."
                                                                    delegate:nil
                                                           cancelButtonTitle:@"Ok"
                                                         positiveButtonTitle:nil];
                    [alertView show];
                    // Redirect back to main screen
                    [self performSegueWithIdentifier:@"termsToInitial" sender:self];
                    
                } else {
                    NSString *errorString = [error userInfo][@"error"];
                    NSLog(@"%@", errorString);
                    
                    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Registration Error"
                                                                     message:errorString
                                                                    delegate:self
                                                           cancelButtonTitle:@"Ok"
                                                         positiveButtonTitle:nil];
                    [alertView show];
                    [self.navigationController popViewControllerAnimated:NO];
                    
                }
            }];

        } else {
            // User chose to not use email address
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    self.alertView = [DTAlertView alertViewUseBlock:block title:@"Confirm Email"
                                                    message:[NSString stringWithFormat:@"Is %@ the email address you would like to use?", [self email]]
                                          cancelButtonTitle:@"No" positiveButtonTitle:@"Yes"];
    [self.alertView show];
    
}

- (BOOL)arePasswordFieldsTheSame {
    if ([self.passwordField.text isEqualToString:self.passwordField2.text]) {
        return YES;
    } else {
        NSLog(@"Password fields are not the same during registration");
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Password Error"
                                                         message:@"Password fields must be the same"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                             positiveButtonTitle:nil];
        [alertView show];
        self.passwordField.text = @"";
        self.passwordField2.text = @"";
        
        return NO;
    }
    
}


// Helper method to determine that ALL relevant fields are valid.
- (BOOL)areAllFieldsValid {
    
    if ([self isFieldValid:self.usernameField]) {
        
        if ([self isFieldValid:self.emailField]) {
            
            if ([self isFieldValid:self.passwordField]) {
                
                if ([self isFieldValid:self.passwordField2]) {
                    return true;
                } else {
                    NSLog(@"Second passwordField is not valid");
                    return false;
                }
            } else {
                NSLog(@"passwordField is not valid.");
                return false;
            }
        } else {
            NSLog(@"emailField of %@ is not valid.", self.emailField.text);
            return false;
        }
    } else {
        NSLog(@"usernameField of %@ is not valid.",self.usernameField.text);
        return false;
        
    }
}

// Helper method to determine validity of a field
- (BOOL)isFieldValid:(UITextField *) field {
    if (![field.text isEqualToString:@""]) {
        
        return TRUE;
    }else{
        
        return FALSE;
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

#pragma mark - textfield methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.usernameField) {
		[textField resignFirstResponder];
		[self.emailField becomeFirstResponder];
	}
    if (textField == self.emailField) {
		[textField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    } else if(textField == self.passwordField) {
        [textField resignFirstResponder];
        [self.passwordField2 becomeFirstResponder];
    } else if(textField == self.passwordField2) {
        [textField resignFirstResponder];
        [self advanceToTermsScene:nil];
    } else {
        [textField resignFirstResponder];
    }
	return YES;
}

@end
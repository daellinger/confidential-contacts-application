//
//  CodeNameViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse.h>
#import "CodeName.h"
#import "SingleCodeNameViewController.h"
#import "Helper.h"
#import <iAd/iAd.h>

@interface CodeNameViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,ADBannerViewDelegate>



@property (strong, nonatomic) IBOutlet UITableView *codeNameTableView;
- (IBAction)addCodename:(UIBarButtonItem *)sender;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;


@end

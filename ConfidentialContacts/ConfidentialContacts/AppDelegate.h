//
//  AppDelegate.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/16/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *globalConnectionToMessagesDict;

@end

//
//  MessageCell.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/22/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *otherCodeName;

@property (strong, nonatomic) IBOutlet UILabel *userCodeName;
@property (strong, nonatomic) IBOutlet UILabel *message;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *otherDescription;
@property (strong, nonatomic) IBOutlet UILabel *userDescription;


@end

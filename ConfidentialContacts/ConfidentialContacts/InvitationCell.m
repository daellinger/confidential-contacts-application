//
//  InvitationCell.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/18/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "InvitationCell.h"

@implementation InvitationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    [Helper makeTextViewRounded:self.codeNameDescription];

}

- (IBAction)declineInvitation:(id)sender {
}

- (IBAction)acceptInvitation:(id)sender {
}

@end

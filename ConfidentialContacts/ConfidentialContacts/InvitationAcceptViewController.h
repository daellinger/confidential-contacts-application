//
//  InvitationAcceptViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/19/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse.h>
#import <GCPlaceholderTextView.h>
#import "CodeName.h"
#import "Helper.h"
#import "DTAlertView.h"
#import <iAd/iAd.h>

@interface InvitationAcceptViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, ADBannerViewDelegate>

@property (strong, nonatomic) PFObject *acceptedInvitation;

@property (strong, nonatomic) NSArray *codeNames;
@property (strong, nonatomic) CodeName *selectedCodeName;

@property (strong, nonatomic) IBOutlet UIPickerView *codeNamePickerView;
@property (strong, nonatomic) IBOutlet UITextView *selectedCodeNameDescription;

- (IBAction)acceptInvitation:(id)sender;
- (IBAction)backToInvitations:(id)sender;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;

@end

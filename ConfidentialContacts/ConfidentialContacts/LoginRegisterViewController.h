//
//  LoginRegisterViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/17/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <iAd/iAd.h>
#import "DTAlertView.h"
#import "AutoHideKeyboardViewController.h"

@interface LoginRegisterViewController : AutoHideKeyboardViewController <ADBannerViewDelegate, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;
@property (strong, nonatomic) IBOutlet UISwitch *usernameSwitch;
@property (strong, nonatomic) IBOutlet UIImageView *mainLogoImage;
@property (strong, nonatomic) IBOutlet UIButton *signInButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UIButton *resetPasswordButton;


- (IBAction)signIn:(id)sender;
- (IBAction)forgotPassword:(id)sender;

@end

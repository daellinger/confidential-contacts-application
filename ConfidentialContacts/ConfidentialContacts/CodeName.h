//
//  CodeName.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CodeName : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *codeNamedescription;
@property (nonatomic) NSString *parseObjectId;

@end

//
//  SingleContactTableViewCell.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/10/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleContactTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *key;
@property (strong, nonatomic) IBOutlet UITextField *value;
//@property (strong, nonatomic) IBOutlet UITextField *firstNameValue;

@end
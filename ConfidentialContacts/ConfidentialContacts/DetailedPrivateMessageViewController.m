//
//  DetailedPrivateMessageViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/23/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "DetailedPrivateMessageViewController.h"
#import "JSMessage.h"
#import "AppDelegate.h"

@interface DetailedPrivateMessageViewController ()

@property (strong, nonatomic) PFObject *loggedInUserCodeName;
@property (strong, nonatomic) PFObject *otherUserCodeName;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (strong, nonatomic) PulldownMenu *pulldownMenu;

@end

@implementation DetailedPrivateMessageViewController

- (void)viewDidLoad {
   
    self.delegate = self;
    self.dataSource = self;
    self.appDelegate = [UIApplication sharedApplication].delegate;
    [super viewDidLoad];
    
    [self setLoggedInUserAndOtherUser];
    // Retrieve messages from Parse and also get codeNames assigned to class properties
    self.selectedMessages = [[NSMutableArray alloc] initWithArray:[self.appDelegate.globalConnectionToMessagesDict valueForKey:[self.selectedConnection objectId]]];
    self.messages = [self convertParseMessagesIntoJSMessages:self.selectedMessages];
    
    [self setupPulldownMenu];
	// Do any additional setup after loading the view.
    
    [[JSBubbleView appearance] setFont:[UIFont systemFontOfSize:16.0f]];
    
    self.messageInputView.textView.placeHolder = @"New Message";
    self.sender = self.otherUserCodeName[@"codeName"];
    
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(reloadTable) userInfo:nil repeats:YES];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	// Do any additional setup after loading the view.
    
    [Helper changePrivateNavigationBarTitle :self:self.otherUserCodeName[@"codeName"]];
    [self scrollToBottomAnimated:NO];
    
}

#pragma mark - Messages view delegate

- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath {
    JSMessage *message = self.messages[(NSUInteger) indexPath.row];
    
    if ([message.sender isEqualToString:self.loggedInUserCodeName[@"codeName"]]) {
        return JSBubbleMessageTypeOutgoing;
    } else {
        return JSBubbleMessageTypeIncoming;
    }
    
}

- (UIImageView *)bubbleImageViewWithType:(JSBubbleMessageType)type forRowAtIndexPath:(NSIndexPath *)indexPath {
    JSMessage *message = self.messages[(NSUInteger) indexPath.row];
    
    if ([message.sender isEqualToString:self.loggedInUserCodeName[@"codeName"]]) {
        return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                          color:[UIColor js_bubbleBlueColor]];
    }
    
    return [JSBubbleImageViewFactory bubbleImageViewForType:type
                                                      color:[UIColor js_bubbleLightGrayColor]];
    
}

- (JSMessageInputViewStyle)inputViewStyle {
    return JSMessageInputViewStyleFlat;
}

#pragma mark - Messages view delegate: OPTIONAL

- (BOOL)shouldDisplayTimestampForRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

//  *** Implement to prevent auto-scrolling when message is added
//
- (BOOL)shouldPreventScrollToBottomWhileUserScrolling {
    return NO;
}

// *** Implemnt to enable/disable pan/tap todismiss keyboard
//
- (BOOL)allowsPanToDismissKeyboard {
    return YES;
}


#pragma mark - Timer Method

- (void)reloadTable {
    
    int count = [[self.appDelegate.globalConnectionToMessagesDict valueForKey:[self.selectedConnection objectId]] count];
    
    if ( !([[NSNumber numberWithInt:[self.selectedMessages count]] isEqualToNumber: [NSNumber numberWithInt:count]])) {
        NSLog(@"Timer Method | Update the table, there is new data!");
        self.selectedMessages = [self.appDelegate.globalConnectionToMessagesDict valueForKey:[self.selectedConnection objectId]];
        self.messages = [self convertParseMessagesIntoJSMessages:self.selectedMessages];
        [self.tableView reloadData];
        [self scrollToBottomAnimated:NO];
        
        // As a precaution in case the global variable has not been updated, update it now!
        int newCount = [[self.appDelegate.globalConnectionToMessagesDict valueForKey:[self.selectedConnection objectId]] count];
        
        // Not equal to
        if ( !([[NSNumber numberWithInt:[self.selectedMessages count]] isEqualToNumber: [NSNumber numberWithInt:newCount]]) ) {
            self.appDelegate.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];

        }
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark - Parse methods

- (void)setLoggedInUserAndOtherUser {
    //Pull code names from connections
    PFObject *inviterCodeName = self.selectedConnection[@"Inviter"];
    PFObject *inviteeCodeName = self.selectedConnection[@"Invitee"];
    
    PFUser *loggedInUser = [PFUser currentUser];
    
    // Pull the users from the codenames
    PFObject *inviterUser = inviterCodeName[@"user"];
    PFObject *inviteeUser = inviteeCodeName[@"user"];
    
    self.loggedInUserCodeName = ( [[inviterUser objectId] isEqualToString:[loggedInUser objectId]] ? inviterCodeName : inviteeCodeName);
    self.otherUserCodeName = ( [[inviteeUser objectId] isEqualToString:[loggedInUser objectId] ] ? inviterCodeName  : inviteeCodeName);
    NSLog(@"loggedInUserCodeName: %@", self.loggedInUserCodeName[@"codeName"]);
    NSLog(@"otherUserCodeName: %@", self.otherUserCodeName[@"codeName"]);

}

- (NSMutableArray *)retrieveMessagesFromConnectionObject {
    NSLog(@"retrieveMessagesFromConnectionObject method");
    [self setLoggedInUserAndOtherUser];
    
    PFRelation *messagesRelation = [self.selectedConnection relationForKey:@"messages"];
    PFQuery *latestMessageQuery = [messagesRelation query];
    [latestMessageQuery includeKey:@"from"];
    [latestMessageQuery addAscendingOrder:@"createdAt"];
    latestMessageQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    // Perhaps store 'messages' to pass into detailed message view so I don't need to query again
    NSMutableArray *messages = [[NSMutableArray alloc] initWithArray:[latestMessageQuery findObjects]];
    NSLog(@"Retrieved %lu messages from Parse", (unsigned long)[messages count]);
    
    return messages;
}

- (NSMutableArray *)convertParseMessagesIntoJSMessages:(NSMutableArray *)parseMessages {
    NSLog(@"entered convertParseMessagesIntoJSMessages method");
    
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    for (PFObject *message in parseMessages) {
        PFObject *codeName = message[@"from"];
        PFObject *cn = [codeName fetchIfNeeded];
        JSMessage *jsMsg = [[JSMessage alloc] initWithText:message[@"message"] sender:cn[@"codeName"] date:message.createdAt];
        [temp addObject:jsMsg];
    }

    return temp;
}

- (JSMessage *)convertParseMessageIntoJSMessage:(PFObject *)parseMessage {
    NSLog(@"Entered convertParseMessageIntoJSMessage");
    
    PFObject *codeName = parseMessage[@"from"];
    PFObject *cn = [codeName fetchIfNeeded];
    JSMessage *jsMsg = [[JSMessage alloc] initWithText:parseMessage[@"message"] sender:cn[@"codeName"] date:parseMessage.createdAt];
    
    return jsMsg;
}

- (id<JSMessageData>)messageForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.messages objectAtIndex:[indexPath row]];
}

- (UIImageView *)avatarImageViewForRowAtIndexPath:(NSIndexPath *)indexPath sender:(NSString *)sender {
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return self.messages.count;
}

- (void)didSendText:(NSString *)text fromSender:(NSString *)sender onDate:(NSDate *)date {
    int remainingMessages = [[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue];
    NSLog(@"Entered didSentText method. sender of text: %@", sender);
    NSLog(@"User has %d messages left", remainingMessages);
    // Check to see if the user has enough messages
    if ( remainingMessages > 0) {
        if ((self.messages.count - 1) % 2) {
            [JSMessageSoundEffect playMessageSentSound];
        }
        
        PFObject *chatMessage = [PFObject objectWithClassName:@"Chat"];
        chatMessage[@"message"] = text;
        chatMessage[@"from"] = self.loggedInUserCodeName;
        chatMessage[@"to"] = self.otherUserCodeName;
        chatMessage[@"fromObjectId"] = [self.loggedInUserCodeName objectId];
        chatMessage[@"toObjectId"] = [self.otherUserCodeName objectId];
        
        
        
        [chatMessage saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Successfully saved chatMessage");
                
                PFRelation *relation = [self.selectedConnection relationforKey:@"messages"];
                [relation addObject:chatMessage];
                [self.selectedConnection saveInBackgroundWithBlock:^(BOOL succeeded2, NSError *error) {
                    if (succeeded2) {
                        NSLog(@"Saved message!");
                        
                        
                        
                        //Send Parse push
                        NSString *channel = [[NSString alloc] initWithFormat:@"a%@", self.otherUserPFObjectId]; // channel must start with a letter, just appending 'a' to the channel name

                        PFPush *push = [[PFPush alloc] init];
                        [push setChannel:channel];
                        [push setData:[[NSDictionary alloc]init]]; // Send empty dictionary -- pushes are used to let the app know that there is something new to query
                        [push sendPushInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (succeeded) {
                                NSLog(@"Successfully sent push message to %@ channel", channel);
                            } else {
                                NSLog(@"Could not send push message to %@ channel due to error: %@ ", channel, error);
                            }
                        }];
                        
                        [self.messages addObject:[self convertParseMessageIntoJSMessage:chatMessage]];
                       // [self.selectedMessages addObject:chatMessage];
                        self.appDelegate.globalConnectionToMessagesDict = [Helper generateDictionaryOfConnectionKeysWithAssociatedMessages];
                        
                        [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@-1];
                        NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
                        [[PFUser currentUser] saveEventually];
                        
                        [self finishSend];
                        [self scrollToBottomAnimated:YES];
                    } else {
                        NSLog(@"Error saving connection object (that contains the message)");
                    }
                }];
            } else {
                NSLog(@"Error saving chatMessage %@", error);
            }
            
        }];
    } else {
        [self outOfMessagesPrompt];
    }

}

- (void)outOfMessagesPrompt {
    
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Out of messages" message:@"You do not have any messages left to send. Go to the menu to purchase more." delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    [alertView show];
}

#pragma mark - Menu Methods

- (IBAction)menuButtonPressed:(id)sender {
    [self.pulldownMenu animateDropDown];
}


- (void)setupPulldownMenu {
    self.pulldownMenu = [[PulldownMenu alloc] initWithView:self.view];
    [self.view addSubview:self.pulldownMenu];
    
    [Helper pulldownMenuStyles:self.pulldownMenu];
    
    [self.pulldownMenu insertButton:@"Show Remaining Messages"];
    [self.pulldownMenu insertButton:@"Purchase 1000 Messages"];
    [self.pulldownMenu insertButton:@"Purchase 2500 Messages"];
    [self.pulldownMenu insertButton:@"Purchase 5000 Messages"];
    [self.pulldownMenu insertButton:@"Delete Messages"];
    [self.pulldownMenu insertButton:@"Delete Connection"];
    [self.pulldownMenu insertButton:@"Logout"];
    
    self.pulldownMenu.delegate = self;
    
    [self.pulldownMenu loadMenu];
}

- (void)menuItemSelected:(NSIndexPath *)indexPath {
    
    switch (indexPath.item) {
        case 0:
            [self showRemainingMessages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 1:
            [self purchase1000Messages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 2:
            [self purchase2500Messages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 3:
            [self purchase5000Messages:nil];
            [self.pulldownMenu animateDropDown];
        case 4:
            [self confirmDeleteMessages:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 5:
            [self confirmDeleteConnection:nil];
            [self.pulldownMenu animateDropDown];
            break;
        case 6:
            [self userLogOutandSegueLogic:nil];
            break;
        default:
            break;
    }
}

- (void)pullDownAnimated:(BOOL)open {
    
    if (open) {
        NSLog(@"Pull down menu open!");
    } else {
        NSLog(@"Pull down menu closed!");
    }
}

#pragma mark - show remaining messages

- (void)showRemainingMessages:(id)sender {
    NSNumber *remainingMessages = [[PFUser currentUser] objectForKey:@"remainingMessages"];
    
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Remaining Messages" message:[NSString stringWithFormat:@"You have %@ remaining messages. You will not be able to send messages if you get to zero.", remainingMessages] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];

    [alertView show];
}

#pragma mark - Purchase Messages

- (void)purchase1000Messages:(id)sender {
    NSLog(@"Entered purchase1000Messages method");
    [PFPurchase buyProduct:@"1000Messages" block:^(NSError *error) {
        if (error) {
            [self showPurchaseMessageError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@1000];
            NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                        delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];

        }
    }];
}

- (void)purchase2500Messages:(id)sender {
    NSLog(@"Entered purchase2500Messages method");
       [PFPurchase buyProduct:@"2500Messages" block:^(NSError *error) {
        if (error) {
            [self showPurchaseMessageError:error];
        } else {
            [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@2500];
            NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
            [[PFUser currentUser] saveEventually];
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                             message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                                                            delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];

        }
    }];


}

- (void)purchase5000Messages:(id)sender {
    NSLog(@"Entered purchase5000Messages method");
    [PFPurchase buyProduct:@"5000Messages" block:^(NSError *error) {
    if (error) {
        [self showPurchaseMessageError:error];
    } else {
        [[PFUser currentUser] incrementKey:@"remainingMessages" byAmount:@5000];
        NSLog(@"Current user has %@ remaining messages left.", [[PFUser currentUser] objectForKey:@"remainingMessages"]);
        [[PFUser currentUser] saveEventually];
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Success"
                                                         message:[NSString stringWithFormat:@"You now have %d messages available!",[[[PFUser currentUser] objectForKey:@"remainingMessages"] intValue]]
                                                        delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
        
    }
}];
    
}

- (void)showPurchaseMessageError:(NSError *)error{
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error purchasing Messages" message:[NSString stringWithFormat:@"Could not purchase messages. Error: %@",[error description]] delegate:self cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
    [alertView show];
}

#pragma mark - Delete Messages

- (void)confirmDeleteMessages:(id)sender {
    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        
        if ([_alertView.clickedButtonTitle isEqualToString:@"Yes"]) {
            [self deleteMessages];
        }
    };
    DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Delete messages" message:@"Delete all messages" cancelButtonTitle:@"No" positiveButtonTitle:@"Yes"];
    [alertView show];
}

- (void)deleteMessages {
    NSLog(@"entered Delete Messages");
    [PFObject deleteAll:self.selectedMessages];
    if ([self.messages count] > 0){
        [self.messages removeAllObjects];
    }
   
    [self.appDelegate.globalConnectionToMessagesDict setValue:nil forKey:[self.selectedConnection objectId]];
    [self.tableView reloadData];

}

#pragma mark - Delete Connection

- (void)confirmDeleteConnection:(id)sender {
    DTAlertViewButtonClickedBlock block = ^(DTAlertView *_alertView, NSUInteger buttonIndex, NSUInteger cancelButtonIndex){
        if ([_alertView.clickedButtonTitle isEqualToString:@"Yes"]) {
            [self deleteConnection];
        }
    };
    DTAlertView *alertView = [DTAlertView alertViewUseBlock:block title:@"Delete Conversation"
                                                    message:[NSString stringWithFormat:@"Completely remove this conversation? A new invitation will need to be issued."]
                                          cancelButtonTitle:@"No" positiveButtonTitle:@"Yes"];
    [alertView show];
    
}

- (void)deleteConnection {
    NSLog(@"Entered deleteConnection method");
    NSMutableArray *parseObjectsToDelete = [[NSMutableArray alloc]initWithArray:self.selectedMessages];
    [parseObjectsToDelete addObject:self.selectedConnection];
    NSString *connectionObjectIdThatNeedsToBeUnsubscribed = [self.selectedConnection objectId];
                                            
    NSLog(@"Entered deleteConversation method");
    [PFObject deleteAllInBackground:parseObjectsToDelete block:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
            NSLog(@"The connection has been deleted (Connection PFObject and all Message PFObjects related)");
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            NSLog(@"Could not delete the connection");
        }

    }];
     
}

#pragma mark - logout

- (void)userLogOutandSegueLogic:(id)sender {
    [Helper logOutAndresetAppToHome:self.view.window];
}

@end
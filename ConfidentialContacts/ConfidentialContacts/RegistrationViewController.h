//
//  RegistrationViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/24/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <iAd/iAd.h>
#import "DTAlertView.h"
#import "AutoHideKeyboardViewController.h"

@interface RegistrationViewController : AutoHideKeyboardViewController <ADBannerViewDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField2;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;
@property (strong, nonatomic) IBOutlet UIWebView *tocWebView;
@property (strong, nonatomic) IBOutlet UIImageView *mainLogoImage;

- (IBAction)acceptRegistration:(UIButton *)sender;
- (IBAction)advanceToTermsScene:(UIButton *)sender;
- (IBAction)switchTocLanguage:(UISegmentedControl *)sender;

@end

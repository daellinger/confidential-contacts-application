//
//  AddCodeNameViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import <GCPlaceholderTextView.h>
#import "DTAlertView.h"
#import <Parse.h>
#import "Helper.h"
#import <iAd/iAd.h>

@interface AddCodeNameViewController : UIViewController <ADBannerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *codenameTextField;
@property (strong, nonatomic) IBOutlet GCPlaceholderTextView *descriptionTextView;
@property (strong, nonatomic) NSArray *codeNames;

- (IBAction)backButton:(UIBarButtonItem *)sender;
- (IBAction)saveCodeName:(UIBarButtonItem *)sender;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;

@end

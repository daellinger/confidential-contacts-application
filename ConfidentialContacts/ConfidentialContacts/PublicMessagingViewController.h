//
//  PublicMessagingViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/22/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicMessagingViewController : UIViewController <UIApplicationDelegate>

@end

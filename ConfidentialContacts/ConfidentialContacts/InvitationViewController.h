//
//  InvitationViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InvitationCell.h"
#import <Parse.h>
#import "Helper.h"
#import "DTAlertView.h"
#import "InvitationAcceptViewController.h"
#import <iAd/iAd.h>

@interface InvitationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,ADBannerViewDelegate>

- (IBAction)addInvitation:(UIBarButtonItem *)sender;
@property (strong, nonatomic) IBOutlet UITableView *invitationTableView;
@property (strong, nonatomic) IBOutlet InvitationCell *invitationCell;

@property (strong, nonatomic) IBOutlet ADBannerView *banner;

@end

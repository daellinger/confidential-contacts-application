//
//  InvitationAcceptDeclineButton.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/26/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//
// This class is used to simply store the inviterCodeName object

#import <UIKit/UIKit.h>
#import <Parse.h>

@interface InvitationAcceptDeclineButton : UIButton

@property (strong, nonatomic) PFObject *inviterCodeName;

@end

//
//  LoginRegisterViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 11/17/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "Helper.h"

@interface LoginRegisterViewController ()

@end

@implementation LoginRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _banner.delegate = self;
    [self saveUsernameToAutoShow];
    // set logo
    UIImage *image = [UIImage imageNamed:@"privateLock.png"];
    [self.mainLogoImage setImage:image];
    
    // Make Buttons have Rounded corners
    [self setButtonToHaveRoundedCorners:self.signInButton];
    [self setButtonToHaveRoundedCorners:self.registerButton];
    [self setButtonToHaveRoundedCorners:self.resetPasswordButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Keyboard Methods

- (void)keyboardWillShow:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -115.0f;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

/**
 If the save username switch is set to on, then the username field will be populated with the last username used.
 It also sets the @c 'Save Username' switch to the appropriate state.
 It is saved as @c NSUserDefault preference
 */
- (void)saveUsernameToAutoShow {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL isUsernameSwitchOn = [prefs boolForKey:@"isUsernameSwitchOn"];
    
    if (isUsernameSwitchOn) {
        [self.usernameSwitch setOn:YES];
        // getting an NSString
        NSString *savedUsername = [prefs stringForKey:@"userName"];
        NSLog(@"Retrieved %@ username from NSUserDefaults", savedUsername);
        self.usernameField.text = savedUsername;
    } else {
        [self.usernameSwitch setOn:NO];
    }

}

/**
 If the  Username Switch is on (the user could have switched it to on at this point), then save the preference and set the text field.
 */
- (void)checkSavedUsernameToPopulateField {
    
    if (self.usernameSwitch.isOn) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefs setBool:self.usernameSwitch.isOn forKey:@"isUsernameSwitchOn"];
        [prefs setObject:self.usernameField.text forKey:@"userName"];
        [prefs synchronize];
    } else {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        [prefs setBool:self.usernameSwitch.isOn forKey:@"isUsernameSwitchOn"];
        [prefs synchronize];
    }

}

- (void)setButtonToHaveRoundedCorners :(UIButton *)button {
    CALayer *btnLayer = [button layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:5.0f];
}

- (IBAction)signIn:(id)sender {
    NSLog(@"entered signIn method");
    //Try to sign in. If success GOTO private, else GOTO public
    if ([self isUserNameFieldValid:self.usernameField] && [self isPasswordFieldValid:self.passwordField]) {
        NSLog(@"fields were valid, attempt to login");
        [PFUser logInWithUsernameInBackground:self.usernameField.text password:self.passwordField.text
                                        block:^(PFUser *user, NSError *error) {
                                            if (user) {
                                                if ([self hasUserAuthenticatedEmail:user]) {
                                                    NSLog(@"Successfully authenticated.");
                                                    NSString *checkpoint = [NSString stringWithFormat:@"%@ has logged into Confidential Contacts (Private)", [user username]];
                                                    [TestFlight passCheckpoint:checkpoint];
                                                    [self checkSavedUsernameToPopulateField];
                                                    
                                                    [Helper subscribeUserToTheirChannel:[user objectId]];
                                                  
                                                    NSLog(@"Segue to private side");
                                                    [self performSegueWithIdentifier:@"initialToPrivate" sender:self];
                                                } else {
                                                    DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Login Error"
                                                                                                     message:@"You need to confirm your registration in your email before logging in."
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                         positiveButtonTitle:nil];
                                                    [alertView show];

                                                }
                                            } else {
                                                //Failed login
                                                NSLog(@"Did not authenticate. Segue to public side");
                                                NSLog(@"Error: %@", error);
                                                if ( ([RHAddressBook authorizationStatus] == RHAuthorizationStatusNotDetermined) ||
                                                    ([RHAddressBook authorizationStatus] == RHAuthorizationStatusDenied)) {
                                                    NSLog(@"Displaying contact access message");
                                                    UIAlertView *alertsuccess = [[UIAlertView alloc] initWithTitle:@"Public Contacts Access"
                                                                                                           message:@"Confidential Contacts will request to access your iOS Device contacts. This is to allow the Public view to display your device contacts. There are no modifications and your data is not saved anywhere. "
                                                                                                          delegate:self
                                                                                                 cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                                    [alertsuccess show];
                                                } else {
                                                    [self performSegueWithIdentifier:@"initialToPublic" sender:self];
 
                                                }

                                                
                                                
                                            }
                                        }];
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        RHAddressBook *ab = [[RHAddressBook alloc] init]; //To trigger authorization before the next screen.
        if ( ([RHAddressBook authorizationStatus] == RHAuthorizationStatusNotDetermined) ||
            ([RHAddressBook authorizationStatus] == RHAuthorizationStatusDenied)) {
            //request authorization
            [ab requestAuthorizationWithCompletion:^(bool granted, NSError *error) {
                if(granted){
                    NSLog(@"Authorized address book access!");
                    [self performSegueWithIdentifier:@"initialToPublic" sender:self];
                } else {
                    NSLog(@"User did not authorize address book access");
                }
            }];
        }
        
    }
}

/*
 Before the user can log into the app, they must authenticate their email address
 */
- (BOOL)hasUserAuthenticatedEmail:(PFUser *)user {
    BOOL emailVerified = [[user valueForKey:@"emailVerified"] boolValue];
    
    if (emailVerified) {
        NSLog(@"user has authenticated email.");
        
        return YES;
    } else {
        NSLog(@"User SHALL NOT PASS. User has not authenticated email");
        
        return NO;
    }
}

- (IBAction)forgotPassword:(id)sender {
    NSLog(@"Entered forgotPassword method");
    UIAlertView *forgotPasswordAlertView = [[UIAlertView alloc] initWithTitle:@"Forgot Password"
                                                           message:@"Enter email address to reset password"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"Reset Password", nil];
    forgotPasswordAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [forgotPasswordAlertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // Do nothing, will automatically close out of window
    } else {
        UITextField *userEmailAddress = [alertView textFieldAtIndex:0];
        NSLog(@"Resetting password for email: %@", userEmailAddress.text);
        [PFUser requestPasswordResetForEmailInBackground:userEmailAddress.text block:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Reset Password Success" message:@"An email has been sent to the email address provided to reset your password." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];

            } else {
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Reset Password Error" message:@"Error resetting the password for the provided email address" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
            }
        }];
    }
}

// Helper method to determine validity of a field
- (BOOL)isUserNameFieldValid:(UITextField *)field {
    
    if ([field.text isEqualToString:@""]) {
        NSLog(@"isUserNameFieldValid returning NO");
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Username missing" message:@"Please enter a username" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
        return NO;
    } else {
        NSLog(@"isFielisUserNameFieldValiddValid returning YES");
        
        return YES;
    }
}

- (BOOL)isPasswordFieldValid:(UITextField *)field {
    if ([field.text length] == 0) {
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Blank Password" message:@"Please enter in password" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
        
        return NO;
    } else {
        return YES;
    }
}


#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

#pragma mark - textfield methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.usernameField) {
		[textField resignFirstResponder];
		[self.passwordField becomeFirstResponder];
	} else if (textField == self.passwordField) {
		[textField resignFirstResponder];
        [self signIn:textField];
	}
    
	return YES;
}



@end

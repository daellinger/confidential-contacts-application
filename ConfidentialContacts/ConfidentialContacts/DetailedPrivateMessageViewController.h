//
//  DetailedPrivateMessageViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/23/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import <Parse.h>
#import "JSMessagesViewController.h"
#import "DTAlertView.h"
#import "PulldownMenu.h"

@interface DetailedPrivateMessageViewController : JSMessagesViewController <JSMessagesViewDataSource, JSMessagesViewDelegate, PulldownMenuDelegate, UIScrollViewDelegate>


@property (strong, nonatomic) PFObject *selectedConnection;

@property (strong, nonatomic) NSMutableArray *messages; // Used to store messages transformed into chat data structure
@property (strong, nonatomic) NSMutableArray *selectedMessages; // Used to store raw parse messages
@property (strong, nonatomic) NSString *otherUserPFObjectId; //Used for push messages

- (IBAction)menuButtonPressed:(id)sender;

@end
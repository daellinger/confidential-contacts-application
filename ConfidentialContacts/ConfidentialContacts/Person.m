//
//  Person.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/2/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "Person.h"

@implementation Person

- (NSComparisonResult)compareFirstName:(Person *)otherObject {
    return [self.firstName compare:otherObject.firstName];
}

@end

//
//  CodeNameViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "CodeNameViewController.h"
#import "AddCodeNameViewController.h"

@interface CodeNameViewController ()

@property (strong, nonatomic) NSArray *tableData;
@property NSInteger rowIndex;

@end

@implementation CodeNameViewController

- (void)viewDidLoad {
    NSLog(@"CodeNameViewController entered viewDidLoad method");
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
     self.tableData = [[NSMutableArray alloc]init];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"CodeNameViewController entered viewWillAppear method");
   [Helper changePrivateNavigationBarTitle:self:@"Code Names"];
   [self getCodeNamesFromParseForLoggedInUser];
    [super viewWillAppear:animated];
    [self.codeNameTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"CodeNameViewController entered viewDidAppear method");
    [self becomeFirstResponder];
}

#pragma mark - Retrieve codenames from user

- (void)getCodeNamesFromParseForLoggedInUser {
    PFQuery *postQuery = [PFQuery queryWithClassName:@"CodeName"];
    [postQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CodeName *cn = [Helper convertPFObjectToCodeName:(PFObject *)obj];
                [temp addObject:cn];
            }];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            [temp sortUsingDescriptors:[NSArray arrayWithObject:sort]];
            self.tableData = temp;
            [self.codeNameTableView reloadData];
            NSLog(@"CodeNameViewController - Loaded %lu code names from Parse for logged in user", (unsigned long)[temp count]);
        } else {
            NSLog(@"CodeNameViewController - Could not find any code names from Parse");
        }
    }];

}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Identifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Create a new CodeName Object
    CodeName *cn = nil;
    
    cn = [self.tableData objectAtIndex:[indexPath row]];

    cell.textLabel.text = [NSString stringWithFormat:@"%@", cn.name];
    
    if (cn.codeNamedescription == nil) {
        cell.detailTextLabel.text = @"";
    } else {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", cn.codeNamedescription];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell was clicked");
    self.rowIndex = indexPath.item; // performForSegue will happen before I can grab the index, so I need to initialize the index into this variable
    [self performSegueWithIdentifier:@"codenameToSingleSegue" sender:tableView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"entered prepareForSegue method");
    
    if ([segue.identifier isEqualToString:@"codenameToSingleSegue" ]) {
        NSLog(@"segue.identifier isEqualToString:@'codenameToSingleSegue'");
        NSLog(@"rowIndex: %ld", (long)self.rowIndex);
        CodeName *cn = [self.tableData objectAtIndex:self.rowIndex];
        
        NSLog(@"CodeName selected: %@", cn.name);
        SingleCodeNameViewController *singleCodeNameViewController = segue.destinationViewController;
        NSLog(@"created singleCodeNameViewController");
        [singleCodeNameViewController setCodeName:cn];
        [singleCodeNameViewController setCodeNames:self.tableData];
        
        NSLog(@"Segue to codenameToSingleSegue");
    }else if([segue.identifier isEqualToString:@"addCodenameSegue"]){
        AddCodeNameViewController *addCodeNameViewController = segue.destinationViewController;
        [addCodeNameViewController setCodeNames:self.tableData];
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

#pragma mark - logout

- (void)userLogOutandSegueLogic:(id)sender {
    [Helper logOutAndresetAppToHome:self.view.window];
} 

@end
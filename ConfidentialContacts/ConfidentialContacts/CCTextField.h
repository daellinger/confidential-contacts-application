//
//  CCTextField.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 4/5/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTextField : UITextField

@end

//
//  PublicMessagingViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/22/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "PublicMessagingViewController.h"

@interface PublicMessagingViewController ()

@end

@implementation PublicMessagingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"Redirect to iOS sms application");
    NSString *stringURL = @"sms:";
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL: url];
}

@end
//
//  SingleContactViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/2/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//
//  General view to display a single person record

#import <UIKit/UIKit.h>
#import "PublicContactsViewController.h"
#import "Person.h"
#import "SingleContactTableViewCell.h"
#import "StaticDataTableViewController.h"
#import "DTAlertView.h"
#import "Helper.h"

@interface SingleContactViewController : StaticDataTableViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) Person *person;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;

// TextFields are embedded in the cell, was just easier to make an outlet
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *homeEmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *workEmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *companyNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *homePhoneNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *workPhoneNumberTextField;
@property (strong, nonatomic) IBOutlet UITextField *cellPhoneNumberTextField;

// Outlet to cells to hide/unhide
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *firstNameCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *lastNameCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *homeEmailCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *workEmailCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *companyNameCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *homePhoneNumberCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *workPhoneNumberCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *cellPhoneNumberCell;
@property (strong, nonatomic) IBOutlet SingleContactTableViewCell *deleteContactCell;


- (IBAction)deleteContact:(UIButton *)sender;

@end

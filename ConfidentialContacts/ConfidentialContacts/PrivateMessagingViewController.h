//
//  PrivateMessagingViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/5/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse.h>
#import "Helper.h"
#import "DTAlertView.h"
#import "MessageCell.h"
#import <iAd/iAd.h>
#import "PulldownMenu.h"

@interface PrivateMessagingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate, PulldownMenuDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) IBOutlet UITableView *messagesTableView;
@property (strong, nonatomic) IBOutlet MessageCell *messageCell;

@property (strong, nonatomic) IBOutlet ADBannerView *banner;
- (IBAction)menuButtonPressed:(id)sender;

@end

//
//  AddInvitationViewController.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/12/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse.h>
#import <AutoHideKeyboardViewController.h>
#import "CodeName.h"
#import "DTAlertView.h"
#import "Helper.h"
#import <iAd/iAd.h>

@interface AddInvitationViewController : AutoHideKeyboardViewController <UIPickerViewDelegate, UIPickerViewDataSource,ADBannerViewDelegate>

@property (strong, nonatomic) NSArray *codeNames;
@property (strong, nonatomic) CodeName *selectedCodeName;

@property (strong, nonatomic) IBOutlet UITextField *inviteeEmailAddress;
@property (strong, nonatomic) IBOutlet UIPickerView *codeNamePickerView;
@property (strong, nonatomic) IBOutlet UITextView *selectedCodeNameDescription;
@property (strong, nonatomic) IBOutlet ADBannerView *banner;

- (IBAction)inviteUser:(id)sender;
- (IBAction)backToInvitations:(UIBarButtonItem *)sender;

@end

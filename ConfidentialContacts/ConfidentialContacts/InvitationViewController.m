//
//  InvitationViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 1/9/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import "InvitationViewController.h"
#import "NSDate+NVTimeAgo.h"
#import "AddInvitationViewController.h"

@interface InvitationViewController ()

@property (strong, nonatomic) NSMutableArray *tableData;
@property NSInteger rowIndex;
@property (strong, nonatomic) PFObject *acceptedInvitation;
@property (strong, nonatomic) NSArray *codeNames;

@end

@implementation InvitationViewController

#pragma - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [Helper changePrivateNavigationBarTitle:self:@"Invitations"];
    [self getInvitationsForLoggedInUser];
    [self getCodeNamesFromParseForLoggedInUser];
    [super viewWillAppear:animated];
    [self.invitationTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
    [Helper checkToSeeIfThereAreAnyPendingConnections:self.tabBarController];
   
}

#pragma mark - Retrieve Invitations from user

- (void)getInvitationsForLoggedInUser {
    NSLog(@"entered getInvitationsForLoggedInUser");
    self.tableData = [[NSMutableArray alloc]init];
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    [postQuery whereKey:@"inviteeUser" equalTo:[PFUser currentUser]];
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:NO]];
    [postQuery includeKey:@"Inviter"];
    [self.tableData addObjectsFromArray:[postQuery findObjects]];
    NSLog(@"InvitationViewController - Loaded %lu connection objects from Parse for logged in user", (unsigned long)[self.tableData count]);
    // Would use 'findObjectsInBackgroundWithBlock' here but since we need the info now and not asynchronously, I am using it this way.
}

#pragma mark - Table View Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableData count];
}

- (InvitationCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSLog(@"Entered cellForRowAtIndexPath");
    static NSString *cellIdentifier = @"Identifier";
    InvitationCell *cell = (InvitationCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // Create a new PFObject Object
    PFObject *connection = nil;
    
    connection = [self.tableData objectAtIndex:[indexPath row]];
    
    PFObject *inviterCodeName = [connection objectForKey:@"Inviter"];
    
    if (inviterCodeName == nil) {
        NSLog(@"Error: Could not retrieve 'Inviter' object from connection object with id %@", [connection objectId]);
        // Try getting invitations again?
        [self getInvitationsForLoggedInUser];
    }
        
    cell.dateLabel.text = [[connection updatedAt] formattedAsTimeAgo];
    
    cell.codeName.text = [NSString stringWithFormat:@"Inviter Code Name: %@", inviterCodeName[@"codeName"]];
    
    if (inviterCodeName[@"description"] == nil) {
        cell.codeNameDescription.text = @"";
    } else {
        cell.codeNameDescription.text = [NSString stringWithFormat:@"%@", inviterCodeName[@"description"]];
    }
    
    cell.acceptButton.inviterCodeName = inviterCodeName;
    cell.declineButton.inviterCodeName = inviterCodeName;
    [cell.declineButton addTarget:self
                           action:@selector(declineInvitationButtonClick:)
                 forControlEvents:UIControlEventTouchUpInside];
    
    [cell.acceptButton addTarget:self
                          action:@selector(acceptInvitationButtonClick:)
                forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell was clicked");
    self.rowIndex = indexPath.item; // performForSegue will happen before I can grab the index, so I need to initialize the index into this variable
   // [self performSegueWithIdentifier:@"privateContactToSingleSegue" sender:tableView];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.textColor = [UIColor whiteColor];
}

#pragma mark - Delete Invitation

- (void)declineInvitationButtonClick:(id)sender {
    NSLog(@"Entered declineInvitationButtonClick");
    PFObject *inviterCodeName = [(InvitationAcceptDeclineButton *) sender inviterCodeName];
    
    if (inviterCodeName == nil) {
        NSLog(@"Error retrieving inviterCodeName, the object is nil");
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error" message:@"There was an error deleting invitation" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
    } else {
        [self declineInvitation:inviterCodeName];
    }
}

- (void)declineInvitation :(PFObject *)inviterCodeName {
    NSLog(@"entered declineInvitation method");
    NSLog(@"inviterCodeName = %@, ", inviterCodeName);
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:NO]];
    [postQuery whereKey:@"Inviter" equalTo:inviterCodeName];
    PFObject *invitation = [postQuery getFirstObject];
    [invitation deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if (succeeded) {
            NSLog(@"Deleted invitation successfully");
            [self getInvitationsForLoggedInUser];
            [self.invitationTableView reloadData];
            
            if ([self.tableData count] > 0) {
                [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%lu", (unsigned long)[self.tableData count]]];
            } else {
                [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
            }
            [Helper checkToSeeIfThereAreAnyPendingConnections:self.tabBarController];
            
        } else {
            NSLog(@"Error deleting invitation: %@", error);
        }
    }];
    

}

#pragma mark - Accept Invitation

- (void)acceptInvitationButtonClick :(id)sender {
    PFObject *inviterCodeName = [(InvitationAcceptDeclineButton *) sender inviterCodeName];
    
    if (inviterCodeName == nil) {
        NSLog(@"Error retrieving inviterCodeName, the object is nil");
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error" message:@"There was an error accepting invitation. The Inviter Code Name was not present." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];

    } else {
        if ( [self.codeNames count] == 0) {
            DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Add code name first" message:@"You must add a code name for yourself before connecting to others." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
            [alertView show];
        } else {
            [self acceptInvitation: sender:[(InvitationAcceptDeclineButton *) sender inviterCodeName]];
        }
        
    }
    
}

- (IBAction)acceptInvitation :(id)sender :(PFObject *)inviterCodeName {
    NSLog(@"Entered acceptInvitation method");
    
    PFQuery *postQuery = [PFQuery queryWithClassName:@"Connection"];
    [postQuery whereKey:@"Active" equalTo:[NSNumber numberWithBool:NO]];
    [postQuery whereKey:@"Inviter" equalTo:inviterCodeName];
    postQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    
    [postQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            // Store accepted invitation so I can pass it to the next view controller
            self.acceptedInvitation = object;
            [self performSegueWithIdentifier:@"InvitationToAcceptSegue" sender:inviterCodeName];
        } else {
            NSLog(@"InvitationViewController - Could not find any connection objects from Parse");
        }
    }];

}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"entered prepareForSegue method");
    
    if ([segue.identifier isEqualToString:@"InvitationToAcceptSegue" ]) {
        NSLog(@"segue.identifier isEqualToString:@'InvitationToAcceptSegue'");
        NSLog(@"accepted invitation objectid: %@", [self.acceptedInvitation objectId]);
        
        InvitationAcceptViewController *invitationAcceptViewController = segue.destinationViewController;
        NSLog(@"created invitationAcceptViewController");
    
        [invitationAcceptViewController setAcceptedInvitation:self.acceptedInvitation];
        NSLog(@"Segue to InvitationToAcceptSegue");
    } else if([segue.identifier isEqualToString:@"addInvitationSegue"]) {
        AddInvitationViewController *addInvitationViewController = segue.destinationViewController;
        [addInvitationViewController setCodeNames:self.codeNames];
    }
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

- (IBAction)addInvitation:(UIBarButtonItem *)sender {
    if( [self.codeNames count] == 0){
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Add code name first" message:@"You must add a code name for yourself before connecting to others." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
    }else{
        [self performSegueWithIdentifier:@"addInvitationSegue" sender:sender];
    }
    
    
}

#pragma mark - Retrieve codenames from user

// Retrieve all code names for the user, and store them in
- (void)getCodeNamesFromParseForLoggedInUser {
    
    self.codeNames = [[NSMutableArray alloc]init];
    PFQuery *postQuery = [PFQuery queryWithClassName:@"CodeName"];
    [postQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    postQuery.cachePolicy = kPFCachePolicyCacheElseNetwork;
    
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Found %lu 'CodeName' objects", (unsigned long)[objects count]);
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            [objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                CodeName *cn = [Helper convertPFObjectToCodeName:obj];
                [temp addObject:cn];
            }];
            
            self.codeNames = temp;
        } else {
            NSLog(@"InvitationViewController - Could not find any code names from Parse");
        }
    }];
}

@end
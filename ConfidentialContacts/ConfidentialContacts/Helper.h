//
//  Helper.h
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/15/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CodeName.h"
#import <Parse.h>
#import "Person.h"
#import "PublicMessagingViewController.h"
#import <RHAddressBook/AddressBook.h>
#import "PulldownMenu.h"

@interface Helper : NSObject


+ (CodeName *)convertPFObjectToCodeName:(PFObject *)personFromParse;
+ (Person *)convertPFObjectToPerson:(PFObject *)personFromParse;

/**
 Log current user out of Parse
 */
+ (void)userLogout;

/**
 Checks to see if there are any pending connections for the logged in user, from the @c Connection table. 
 A Pending Connection is if @c Active is @c false for that connection row, for the current logged in user.
 @param tabBarController
    Need to update the badge icon for the app tabBarController
 */
+ (void)checkToSeeIfThereAreAnyPendingConnections:(UITabBarController *)tabBarController;

+ (void)makeTextViewRounded:(UITextView *)textView;

/**
 Differentiates between Private (logged in user) and normal public view
 @param vc
    UIViewController that this is needed for
 @param navBarTitleText
 */
+ (void)changePrivateNavigationBarTitle:(UIViewController *)vc:(NSString *)navBarTitleText;

/**
 Calls log out method and shows the public messaging view controller"
 */
+ (void)logOutAndresetAppToPublicMessagingViewController:(UIWindow *)window;

+ (void)logOutAndresetAppToHome:(UIWindow *)window;

#pragma mark - Retrieve connections / messages from Parse
+ (NSArray *)getAllConnectionsForLoggedInUser;
+ (NSArray *)getAllMessagesForLoggedInUser:(PFObject *)connection;
+ (NSMutableDictionary *)generateDictionaryOfConnectionKeysWithAssociatedMessages;

#pragma mark - Parse Push methods
+ (void)subscribeUserToTheirChannel:(NSString *)userObjectId;
+ (void)unsubscribeUserFromTheirChannel:(NSString *)userObjectId;

+ (void)pulldownMenuStyles:(PulldownMenu *)menu;

@end
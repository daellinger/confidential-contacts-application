//
//  InvitationAcceptViewController.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 2/19/14.
//  Copyright (c) 2014 David Ellinger. All rights reserved.
//
// Manages view controller for when user accepts an invitation

#import "InvitationAcceptViewController.h"

@interface InvitationAcceptViewController ()

@property (strong, atomic) PFObject *rawSelectedCodeName;

@end

@implementation InvitationAcceptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _banner.delegate = self;
    [Helper makeTextViewRounded:self.selectedCodeNameDescription];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Add this here to reload Codenames every time the view is loaded
    [self getCodeNamesFromParseForLoggedInUser];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSLog(@"number of code name rows: %lu", (unsigned long)[self.codeNames count]);
    
    return [self.codeNames count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    CodeName *cn = [Helper convertPFObjectToCodeName:[self.codeNames objectAtIndex:row]];
    
    return cn.name;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    PFObject *rawCn = [self.codeNames objectAtIndex:row];
    self.rawSelectedCodeName = rawCn;
    
    CodeName *cn = [Helper convertPFObjectToCodeName:rawCn];
    NSLog(@"Selected CodeName: %@", [cn name]);
    [self.selectedCodeNameDescription setText:[cn codeNamedescription]];
    self.selectedCodeName = cn;
}

#pragma mark - Retrieve codenames from user

- (void)getCodeNamesFromParseForLoggedInUser {
    
    self.codeNames = [[NSMutableArray alloc]init];
    
    PFQuery *postQuery = [PFQuery queryWithClassName:@"CodeName"];
    [postQuery whereKey:@"user" equalTo:[PFUser currentUser]];
    
    [postQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            NSLog(@"Found %lu 'CodeName' objects", (unsigned long)[objects count]);
            NSMutableArray *temp = [[NSMutableArray alloc]init];
            
            self.codeNames = objects;
            [self.codeNamePickerView setDelegate:self];
            [self.codeNamePickerView setDataSource:self];
            [self.selectedCodeNameDescription setText:[[self.codeNames firstObject] objectForKey:@"description"]];
            [self setInitialSelectedCodeName];
            NSLog(@"InvitationAcceptViewController - Converted %lu code names from Parse to entity class for logged in user", (unsigned long)[temp count]);
        } else {
            NSLog(@"InvitationAcceptViewController - Could not find any code names from Parse");
        }
    }];
}

/**
 If the user does not scroll in the uipickerview, the first code name needs to be selected. So automatically select it from the beginning.
 */
- (void)setInitialSelectedCodeName {
    
    if ([self.codeNames count] > 0) {
        self.rawSelectedCodeName = [self.codeNames firstObject];
    }
}

- (IBAction)acceptInvitation:(id)sender {
    
    NSLog(@"Entered acceptInvitation method");
    
    
    if (self.rawSelectedCodeName != nil) {
        NSLog(@"Setting invitee column to be: %@", self.rawSelectedCodeName);
        [self.acceptedInvitation setObject:[NSNumber numberWithBool:YES] forKey:@"Active"];
        [self.acceptedInvitation setObject:self.rawSelectedCodeName forKey:@"Invitee"];
        [self.acceptedInvitation setObject:[self.rawSelectedCodeName objectId] forKey:@"inviteeObjectId"];
        // Remove inviteeUser field, no need for it anymore
        [self.acceptedInvitation removeObjectForKey:@"inviteeUser"];
        
        [self.acceptedInvitation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                NSLog(@"Successfully accepted and updated invitation");
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Invitation Accepted" message:@"Connection secured!" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                NSLog(@"Error in accepting invitation. Error: %@", error);
                DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Error accepting invitation" message:@"Could accept invitation." delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
                [alertView show];
            }
        }];
    } else {
        DTAlertView *alertView = [DTAlertView alertViewWithTitle:@"Select Code Name" message:@"Select the code name you would like to use" delegate:nil cancelButtonTitle:@"Ok" positiveButtonTitle:nil];
        [alertView show];
    }
    
}

- (IBAction)backToInvitations:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - shake action

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        NSLog(@"Detected shake action");
        [Helper logOutAndresetAppToPublicMessagingViewController:self.view.window];
    }
}

#pragma mark iAd Delegate Methods

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    // Gets called when iAds is loaded.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:1];
    [UIView commitAnimations];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    // Gets called when iAds can't be loaded (no internet connection).
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    // Set iAd to visible, creates nice fade-in effect
    [banner setAlpha:0];
    [UIView commitAnimations];
}

@end
//
//  SingleContactTableViewCell.m
//  ConfidentialContacts
//
//  Created by David Ellinger on 12/10/13.
//  Copyright (c) 2013 David Ellinger. All rights reserved.
//

#import "SingleContactTableViewCell.h"

@implementation SingleContactTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
